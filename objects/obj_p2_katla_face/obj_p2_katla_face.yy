{
    "id": "d05fe409-efe7-4a56-b393-b424f87ac643",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_katla_face",
    "eventList": [
        {
            "id": "a4ef883c-6982-459d-aa48-653c3c990671",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d05fe409-efe7-4a56-b393-b424f87ac643"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f3b73a3-4c7f-4730-b194-057b97a57eba",
    "visible": true
}