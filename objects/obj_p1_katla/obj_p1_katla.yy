{
    "id": "652ef388-0f7b-43a7-ba62-02611df49ce1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_katla",
    "eventList": [
        {
            "id": "d0c273b6-414e-464f-a5be-6080ab6c1e20",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "652ef388-0f7b-43a7-ba62-02611df49ce1"
        },
        {
            "id": "cef813c5-36aa-4308-9132-749f458bfae4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "652ef388-0f7b-43a7-ba62-02611df49ce1"
        },
        {
            "id": "ce38fc73-46c5-40c4-a85d-afc84b83fe2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "652ef388-0f7b-43a7-ba62-02611df49ce1"
        },
        {
            "id": "238c7530-5d62-4641-9c45-6cd97010874c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "652ef388-0f7b-43a7-ba62-02611df49ce1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bc8ebf16-501c-4419-a3f5-10bdd569017b",
    "visible": true
}