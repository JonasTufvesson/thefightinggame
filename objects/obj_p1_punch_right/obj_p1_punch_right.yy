{
    "id": "bb35f650-1648-492d-839c-fc8b520af16f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_punch_right",
    "eventList": [
        {
            "id": "61b3e30d-26b1-4a6d-a636-56c4377b3e8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb35f650-1648-492d-839c-fc8b520af16f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9505cab-747f-461f-ba5a-ca9302b415b0",
    "visible": false
}