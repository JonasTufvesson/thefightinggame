{
    "id": "b2e0a9fe-a0cb-4167-91f3-f837e8230890",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quit",
    "eventList": [
        {
            "id": "4f3babee-c793-4fe1-a11a-f03ee1cde97f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2e0a9fe-a0cb-4167-91f3-f837e8230890"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b697c81c-1ad6-4a9b-b105-9d045da66ee6",
    "visible": true
}