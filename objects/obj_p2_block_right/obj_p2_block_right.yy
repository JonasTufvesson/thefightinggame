{
    "id": "8428fdd8-76eb-4c64-8792-674ca3443c3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_block_right",
    "eventList": [
        {
            "id": "7a822e75-4eea-4568-8c3f-789960e6a044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8428fdd8-76eb-4c64-8792-674ca3443c3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "289271d2-dd4d-4933-a6fc-ef7d691728d2",
    "visible": false
}