{
    "id": "5f602eef-8762-4658-ae44-398b9bf4f655",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_play",
    "eventList": [
        {
            "id": "4500a7e1-4395-42cc-878d-26023e5fbe75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5f602eef-8762-4658-ae44-398b9bf4f655"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2abb2726-70ca-49ea-a05a-102579a48c0b",
    "visible": true
}