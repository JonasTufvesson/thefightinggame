{
    "id": "2c790e10-32ac-4b4c-9810-3c0302db86d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_kick_left",
    "eventList": [
        {
            "id": "ce16d95c-c5a5-471b-8504-231077358c4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c790e10-32ac-4b4c-9810-3c0302db86d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9505cab-747f-461f-ba5a-ca9302b415b0",
    "visible": false
}