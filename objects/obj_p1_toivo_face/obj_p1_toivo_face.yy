{
    "id": "a49dbb8e-a497-429a-88c5-7d9b8966d93b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_toivo_face",
    "eventList": [
        {
            "id": "5fa3198b-7a87-4c16-a75a-163202e01237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a49dbb8e-a497-429a-88c5-7d9b8966d93b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec33a169-be3f-4c84-8416-833449c9baee",
    "visible": true
}