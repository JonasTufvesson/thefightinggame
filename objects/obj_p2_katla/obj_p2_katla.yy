{
    "id": "c59f8eb4-a085-4630-89cc-cd2024bcaa1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_katla",
    "eventList": [
        {
            "id": "fc96a643-6499-4ce2-b198-eb880918c9ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c59f8eb4-a085-4630-89cc-cd2024bcaa1e"
        },
        {
            "id": "e794e11f-df21-4165-bfc5-fc653dddc986",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c59f8eb4-a085-4630-89cc-cd2024bcaa1e"
        },
        {
            "id": "b51a6105-9a9e-4736-b53c-b9057a31e5ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c59f8eb4-a085-4630-89cc-cd2024bcaa1e"
        },
        {
            "id": "1d263b59-a034-421c-9ccb-13ac388cbb5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c59f8eb4-a085-4630-89cc-cd2024bcaa1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f93f490-5a4a-4e17-8866-b377b189c51f",
    "visible": true
}