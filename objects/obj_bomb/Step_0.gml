/// @description Insert description here
// You can write your code in this editor

if (place_meeting(x,y, obj_base)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)
	
}


// Player 1
if (place_meeting(x,y, obj_p1_jonatan)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p1health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)
}

if (place_meeting(x,y, obj_p1_katla)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p1health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}

if (place_meeting(x,y, obj_p1_hilda)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p1health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}

if (place_meeting(x,y, obj_p1_toivo)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p1health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}

// Player 2
if (place_meeting(x,y, obj_p2_jonatan)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p2health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}
if (place_meeting(x,y, obj_p2_toivo)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p2health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}
if (place_meeting(x,y, obj_p2_hilda)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p2health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}

if (place_meeting(x,y, obj_p2_katla)) {
	instance_create_layer(x,y, "Instances", obj_explosion);
	p2health -=25;
	instance_destroy(obj_bomb, true);
	bombdrop = 0;
	audio_play_sound(snd_bomb, 10, false)

}