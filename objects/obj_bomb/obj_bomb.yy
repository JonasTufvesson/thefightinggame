{
    "id": "1ced0788-bc10-43db-a2d3-7c38d2c0e150",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bomb",
    "eventList": [
        {
            "id": "2c829add-22d1-48ad-8e3f-23c5e621df8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1ced0788-bc10-43db-a2d3-7c38d2c0e150"
        },
        {
            "id": "ebb37a25-459d-49a9-b70f-f1929431e5bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1ced0788-bc10-43db-a2d3-7c38d2c0e150"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "53b24e0b-ea4f-4491-875c-801cb3eaf074",
    "visible": true
}