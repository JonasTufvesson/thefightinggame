{
    "id": "4d05fc9d-80d2-49fd-b272-0532d9573251",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_toivo",
    "eventList": [
        {
            "id": "bc98310c-5b92-4c5d-9dfc-e78b2384721a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d05fc9d-80d2-49fd-b272-0532d9573251"
        },
        {
            "id": "9d10a1b3-ebe0-4871-b854-9f8526b6f871",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d05fc9d-80d2-49fd-b272-0532d9573251"
        },
        {
            "id": "677dcb20-13dc-4252-82be-425c11144849",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4d05fc9d-80d2-49fd-b272-0532d9573251"
        },
        {
            "id": "834366ae-6785-46cc-896b-1807aae5aba3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4d05fc9d-80d2-49fd-b272-0532d9573251"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c7bc5ded-327c-4a0a-9bc3-4028bb589754",
    "visible": true
}