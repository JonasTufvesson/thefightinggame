{
    "id": "b2958151-1858-444e-b8aa-2021da4c00a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_health",
    "eventList": [
        {
            "id": "5f6252c3-25c8-49ef-9b33-8a707067dd28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b2958151-1858-444e-b8aa-2021da4c00a1"
        },
        {
            "id": "8cff4ee9-3382-4188-9d92-335b8445f8de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b2958151-1858-444e-b8aa-2021da4c00a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f989446a-6ffd-45e2-8097-dff958110348",
    "visible": true
}