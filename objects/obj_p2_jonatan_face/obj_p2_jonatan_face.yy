{
    "id": "6933638d-f8ab-49b3-bf01-ae2d2a077283",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_jonatan_face",
    "eventList": [
        {
            "id": "92f902d6-0e8a-4d9f-9839-5c196c7d1aef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6933638d-f8ab-49b3-bf01-ae2d2a077283"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95ca7d98-198c-49eb-bbcf-19fe0a3aaff0",
    "visible": true
}