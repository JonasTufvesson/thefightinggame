{
    "id": "e0f809a4-104f-467b-9c21-60eec71a770d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_hilda_face",
    "eventList": [
        {
            "id": "fcfbb608-d099-4d25-9eaa-4e5b4030b81e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0f809a4-104f-467b-9c21-60eec71a770d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "70ae25f3-e866-4be0-9258-2fcd0f860a04",
    "visible": true
}