/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor


// Character not leaving room
x = clamp(x, 0, 1900);
y = clamp(y, 0, 1020);

// Kick, Block, Punch
if (gamepad_button_check_pressed(4,gp_face2)) || (keyboard_check_pressed (ord("M"))) {
	
	if (sprite_index == jonatan_stand_left) {
		sprite_index = jonatan_punch_left;
	instance_create_layer(x+60,y+25, "Instances", obj_p2_punch_left)
	alarm[0] = room_speed * 0.1;
	} 
	else if (sprite_index == jonatan_stand_right) {
		sprite_index = jonatan_punch_right;
	instance_create_layer(x-50,y+25, "Instances", obj_p2_punch_right)
	alarm[0] = room_speed * 0.1;
	}
}

if (gamepad_button_check_pressed(4, gp_face3)) || (keyboard_check_pressed (ord("K"))) {
	
	if (sprite_index == jonatan_stand_left) {
		sprite_index = jonatan_kick_left;
	instance_create_layer(x+90,y+90, "Instances", obj_p2_kick_left)
	alarm[0] = room_speed * 0.1;
	
	} 
	else if (sprite_index == jonatan_stand_right) {
		sprite_index = jonatan_kick_right;
	instance_create_layer(x-60,y+90, "Instances", obj_p2_kick_right)
	alarm[0] = room_speed * 0.1;
	}
}


if (gamepad_button_check_pressed(4, gp_face4)) || (keyboard_check_pressed (ord("L"))) {
	
	if (sprite_index == jonatan_stand_left) {
		sprite_index = jonatan_block_left;
	instance_create_layer(x+40,y+10, "Instances", obj_p2_block_left)
	alarm[0] = room_speed * 0.1;
	} 
	else if (sprite_index == jonatan_stand_right) {
		sprite_index = jonatan_block_right
	instance_create_layer(x,y+10, "Instances", obj_p2_block_right)
	alarm[0] = room_speed * 0.1;
	}
}




// movement





if (gamepad_axis_value(4, gp_axislh) < 0) {
	x  -= move_speed;
	sprite_index = jonatan_run_right
	}
	
if (gamepad_axis_value(4, gp_axislh) = 0) {
	
	if (sprite_index = jonatan_run_right) {
		sprite_index = jonatan_stand_right
		}
}

if (gamepad_axis_value(4, gp_axislh) > 0) {
	x  += move_speed;
	sprite_index = jonatan_run_left
	}
if (gamepad_axis_value(4, gp_axislh) = 0)  {
	
	if (sprite_index = jonatan_run_left) {
		sprite_index = jonatan_stand_left
		}
	}

	
if (gamepad_axis_value(4, gp_axislv) > 0) {
	
	if (sprite_index == jonatan_stand_right) {
		sprite_index = jonatan_crouch_right;
		alarm[0] = room_speed * 0.1;
		} 
	else if (sprite_index == jonatan_stand_left) {
		sprite_index = jonatan_crouch_left;
		alarm[0] = room_speed * 0.1;
		}
	}

	
if (place_meeting(x, y, obj_base))
   {
  
gravity = 0;
vspeed = 0;
jump =0;
 
}


if (gamepad_axis_value(4, gp_axislv) < 0 && jump <3)  {

	jump = jump+0.1;
	vspeed = -5;
	gravity = 0.1;
	}


// movement keyboard




// Collisions

if (place_meeting(x,y, obj_p1_punch_right)) {
	p2health -=15; 
	x = x-100;
	audio_play_sound(snd_punch, 10, false)
	}

if (place_meeting(x,y, obj_p1_punch_left)) {
	p2health -=15; 
	x = x+100;
	audio_play_sound(snd_punch, 10, false)
	}
	
if (place_meeting(x,y, obj_p1_kick_right)) {
	p2health -=10; 
	x = x-50;
	audio_play_sound(snd_kick, 10, false)
	}

if (place_meeting(x,y, obj_p1_kick_left)) {
	p2health -=10; 
	x = x+50;
	audio_play_sound(snd_kick, 10, false)
	}	
if (place_meeting(x,y, obj_p1_block_left)) { 
	x = x+200;
	audio_play_sound(snd_block, 10, false)
	}	

if (place_meeting(x,y, obj_p1_block_right)) { 
	x = x-200;
	audio_play_sound(snd_block, 10, false)
	}	

if (place_meeting(x, y, obj_fire2)) {
  
	x = xstart;
	y = ystart;
	p2health -=15;
	audio_play_sound(snd_fire, 10, false)
	}
	
// end game

if (p2health <= 0) {
room_goto_next()	
}