{
    "id": "470661cd-8e8a-4dbe-871e-ed34affc3164",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_jonatan",
    "eventList": [
        {
            "id": "f7485094-c940-4ac4-b2e6-244c328b21d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "470661cd-8e8a-4dbe-871e-ed34affc3164"
        },
        {
            "id": "779f19fd-6ffa-48c3-8729-7da47bff637c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "470661cd-8e8a-4dbe-871e-ed34affc3164"
        },
        {
            "id": "ba3ae715-a204-42dc-8fa0-8b365e5f4a77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "470661cd-8e8a-4dbe-871e-ed34affc3164"
        },
        {
            "id": "5a173497-ea66-47c6-b63c-61eb4230b173",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "470661cd-8e8a-4dbe-871e-ed34affc3164"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "879aa995-cb7b-43d6-968f-f8b86d667e17",
    "visible": true
}