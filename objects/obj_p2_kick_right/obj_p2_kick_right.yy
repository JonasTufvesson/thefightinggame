{
    "id": "c422d725-93d8-4b38-83a7-d1945bcefa03",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_kick_right",
    "eventList": [
        {
            "id": "6a777be8-0828-468e-b9ed-3bec9a24cc24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c422d725-93d8-4b38-83a7-d1945bcefa03"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9505cab-747f-461f-ba5a-ca9302b415b0",
    "visible": false
}