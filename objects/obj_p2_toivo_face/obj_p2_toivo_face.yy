{
    "id": "a9640cf7-a95f-4292-8bed-96536c18f30d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_toivo_face",
    "eventList": [
        {
            "id": "5343c5ed-b3af-4cf3-9d2a-6f6bd305566a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a9640cf7-a95f-4292-8bed-96536c18f30d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ec33a169-be3f-4c84-8416-833449c9baee",
    "visible": true
}