{
    "id": "157c1561-54a9-486e-9bff-2dc3c1da67fa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_hilda",
    "eventList": [
        {
            "id": "548ffe97-ba77-4d64-8ac4-69b266337c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "157c1561-54a9-486e-9bff-2dc3c1da67fa"
        },
        {
            "id": "31ef91f8-eaa2-405c-88bf-d8e83aa354c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "157c1561-54a9-486e-9bff-2dc3c1da67fa"
        },
        {
            "id": "9d5d9f66-d29b-4982-92f3-06e61219bb10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "157c1561-54a9-486e-9bff-2dc3c1da67fa"
        },
        {
            "id": "39bf055d-68c4-4f5f-a8c3-1276dac0b3a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "157c1561-54a9-486e-9bff-2dc3c1da67fa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bce96f9c-5738-4930-b5e9-de290daadc1e",
    "visible": true
}