{
    "id": "8208426c-8ae7-43f9-9f62-9e2ef53ae1d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_punch_right",
    "eventList": [
        {
            "id": "0f08943c-150c-48c5-a0bb-883f75ecd89d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8208426c-8ae7-43f9-9f62-9e2ef53ae1d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9505cab-747f-461f-ba5a-ca9302b415b0",
    "visible": false
}