{
    "id": "f94f1d74-b5dc-4bd6-8c2f-da62619ebdcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p2_hilda",
    "eventList": [
        {
            "id": "6a084b28-6eb1-4868-81ba-4193684a37e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f94f1d74-b5dc-4bd6-8c2f-da62619ebdcb"
        },
        {
            "id": "4a81c0b9-5009-4aec-9982-6027f15f80bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f94f1d74-b5dc-4bd6-8c2f-da62619ebdcb"
        },
        {
            "id": "8f0ae83d-6c65-45f7-b671-99133c063f93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f94f1d74-b5dc-4bd6-8c2f-da62619ebdcb"
        },
        {
            "id": "f4de6742-e3a0-448d-98b7-409626509014",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f94f1d74-b5dc-4bd6-8c2f-da62619ebdcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "81b5f2a9-00f0-4f93-a8ec-dd76ca03bf28",
    "visible": true
}