{
    "id": "fa595d61-62e5-43d6-94a0-869c12943056",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_jonatan_face",
    "eventList": [
        {
            "id": "56d1b8c5-c0ba-43c7-b392-31120022156f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fa595d61-62e5-43d6-94a0-869c12943056"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95ca7d98-198c-49eb-bbcf-19fe0a3aaff0",
    "visible": true
}