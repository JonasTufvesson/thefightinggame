{
    "id": "6db4ed76-d30a-4437-819d-cf042c74f42b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_jonatan",
    "eventList": [
        {
            "id": "12f8b46c-454b-4c16-8878-e48660680198",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6db4ed76-d30a-4437-819d-cf042c74f42b"
        },
        {
            "id": "9c437a91-e5a3-4d11-9baa-132e1ee5dbf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6db4ed76-d30a-4437-819d-cf042c74f42b"
        },
        {
            "id": "e1ea70fc-3274-4058-97c6-75f078fb197c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6db4ed76-d30a-4437-819d-cf042c74f42b"
        },
        {
            "id": "593a702f-ee7a-4470-914f-678ac076df91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6db4ed76-d30a-4437-819d-cf042c74f42b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "37ab9ece-d537-41e4-b51a-54bcfdc4812d",
    "visible": true
}