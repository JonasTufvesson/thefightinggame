/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor



// Character not leaving room
x = clamp(x, 0, 1900);
y = clamp(y, 0, 1020);

// Buttons, block, punch, kick
if (gamepad_button_check_pressed(5,gp_face2)) || (keyboard_check_pressed (ord("Z"))) {
	
	if (sprite_index == toivo_stand_right) {
		sprite_index = toivo_punch_right;
		instance_create_layer(x-50,y+30, "Instances", obj_p1_punch_right)
		alarm[0] = room_speed * 0.1;
		} 
	else if (sprite_index == toivo_stand_left) {
		sprite_index = toivo_punch_left;
		instance_create_layer(x+70,y+30, "Instances", obj_p1_punch_left)
		alarm[0] = room_speed * 0.1;
		} 	
}
	
if (gamepad_button_check_pressed(5, gp_face3)) || (keyboard_check_pressed (ord("X"))) {
	
	if (sprite_index == toivo_stand_left) {
		sprite_index = toivo_kick_left;
		instance_create_layer(x+90,y+80, "Instances", obj_p1_kick_left)
		alarm[0] = room_speed * 0.1;
		} 
	else if (sprite_index == toivo_stand_right) {
		sprite_index = toivo_kick_right;
		instance_create_layer(x-60,y+80, "Instances", obj_p1_kick_right)
		alarm[0] = room_speed * 0.1;
		}
	}

if (gamepad_button_check_pressed(5, gp_face4)) || (keyboard_check_pressed (ord("C"))) {
	
	if (sprite_index == toivo_stand_left) {
		sprite_index = toivo_block_left;
		instance_create_layer(x+40,y+10, "Instances", obj_p1_block_left)
		alarm[0] = room_speed * 0.1;
		} 
	else if (sprite_index == toivo_stand_right) {
		sprite_index = toivo_block_right;
		instance_create_layer(x,y+10, "Instances", obj_p1_block_right)
		alarm[0] = room_speed * 0.1;
		}
	}

// Movement

if (gamepad_button_check(5,gp_padl)) || (keyboard_check_direct(ord("A"))) {
	x  -= move_speed;
	sprite_index = toivo_run_right
	}


if (gamepad_button_check_released(5, gp_padl)) || (keyboard_check_released (ord("A"))) {
	sprite_index = toivo_stand_right	
	}


if (gamepad_button_check(5,gp_padr)) || (keyboard_check_direct (ord("D"))) {
	x  += move_speed;
	sprite_index = toivo_run_left
	}

if (gamepad_button_check_released(5, gp_padr)) || (keyboard_check_released (ord("D"))) {
	sprite_index = toivo_stand_left	
	}


	
if (gamepad_button_check(5, gp_padd)) || (keyboard_check_direct(ord("S"))) {
	
	if (sprite_index == toivo_stand_left) {
		sprite_index = toivo_crouch_left;
		alarm[0] = room_speed * 0.1;
	} 
	else if (sprite_index == toivo_stand_right) {
		sprite_index = toivo_crouch_right;
		alarm[0] = room_speed * 0.1;
	}
}

	
if (place_meeting(x, y, obj_base)) {
  
	gravity = 0;
	vspeed = 0;
	jump =0;
	}


if (gamepad_button_check_pressed(5,gp_padu)) && (jump <4) || (keyboard_check_pressed (ord("W")))  && (jump < 4){

	jump = jump+0.1;
	vspeed = -7;
	gravity = 0.1;
	}



// Collisions
if (place_meeting(x,y, obj_p2_punch_right)) {
	p1health -=15; 
	x = x-100;
	audio_play_sound(snd_punch, 10, false)
	}

if (place_meeting(x,y, obj_p2_punch_left)) {
	p1health -=15; 
	x = x+100;
	audio_play_sound(snd_punch, 10, false)
	}
	
if (place_meeting(x,y, obj_p2_kick_right)) {
	p1health -=10; 
	x = x-50;
	audio_play_sound(snd_kick, 10, false)
	}

if (place_meeting(x,y, obj_p2_kick_left)) {
	p1health -=10; 
	x = x+50;
	audio_play_sound(snd_kick, 10, false)
	}	
if (place_meeting(x,y, obj_p2_block_left)) { 
	x = x+200;
	audio_play_sound(snd_block, 10, false)
	
	}	

if (place_meeting(x,y, obj_p2_block_right)) { 
	x = x-200;
	audio_play_sound(snd_block, 10, false)
	}	

if (place_meeting(x, y, obj_fire2)) {
  
	x = xstart;
	y = ystart;
	p1health -=15;
	audio_play_sound(snd_fire, 10, false)
	}
	
// end game

if (p1health <= 0) {
room_goto_next()	
}

