{
    "id": "2d2c1e82-56a6-4f3d-a6ae-c26c4fe1f93c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_toivo",
    "eventList": [
        {
            "id": "0f434f34-791e-408f-b33c-5add19d40ee6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2d2c1e82-56a6-4f3d-a6ae-c26c4fe1f93c"
        },
        {
            "id": "3f886d49-0d7a-4f15-a7ef-23421783d059",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2d2c1e82-56a6-4f3d-a6ae-c26c4fe1f93c"
        },
        {
            "id": "df6eeeb7-e0a3-49e1-8ba0-f4d1fef12c82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2d2c1e82-56a6-4f3d-a6ae-c26c4fe1f93c"
        },
        {
            "id": "92297adf-6f3d-47f0-b677-7fba6fe7e526",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "2d2c1e82-56a6-4f3d-a6ae-c26c4fe1f93c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5b6604fd-71c0-4be9-976a-1875c3322aaf",
    "visible": true
}