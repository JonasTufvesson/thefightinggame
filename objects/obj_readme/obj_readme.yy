{
    "id": "25f120a2-7abd-4658-b26c-731d89561a72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_readme",
    "eventList": [
        {
            "id": "1033b355-4ecb-49b4-abb6-eae193b94031",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "25f120a2-7abd-4658-b26c-731d89561a72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "39e516d6-a4e5-43a7-b7f1-68b497d62c12",
    "visible": true
}