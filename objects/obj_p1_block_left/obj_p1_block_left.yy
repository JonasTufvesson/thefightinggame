{
    "id": "12fce1ff-2353-4e94-bed7-265a77558125",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_p1_block_left",
    "eventList": [
        {
            "id": "5e69b0fb-b0a3-4586-ac86-e98688770199",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12fce1ff-2353-4e94-bed7-265a77558125"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "289271d2-dd4d-4933-a6fc-ef7d691728d2",
    "visible": false
}