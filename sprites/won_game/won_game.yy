{
    "id": "725d0487-0e24-4fba-ac42-a773659f15a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "won_game",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1979,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48c09c06-49b3-4e8a-a2bc-469f2b264ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "725d0487-0e24-4fba-ac42-a773659f15a8",
            "compositeImage": {
                "id": "f796c876-fdf3-4105-99bb-52f04c09b8b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48c09c06-49b3-4e8a-a2bc-469f2b264ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64a46793-f5ae-4e09-839d-91a0a88fffa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c09c06-49b3-4e8a-a2bc-469f2b264ecd",
                    "LayerId": "8688627c-3829-4ec7-9f48-c9e54fa51ba7"
                },
                {
                    "id": "e04b42d9-ba18-44c2-b12c-c66a79aad662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c09c06-49b3-4e8a-a2bc-469f2b264ecd",
                    "LayerId": "7cf89a58-5158-44d6-923d-9c458ecdce0c"
                },
                {
                    "id": "343a09b9-f5bf-48b1-bf26-0f977d419eb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48c09c06-49b3-4e8a-a2bc-469f2b264ecd",
                    "LayerId": "92084eed-ec42-44ed-b3c4-b5cf589fc863"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "92084eed-ec42-44ed-b3c4-b5cf589fc863",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "725d0487-0e24-4fba-ac42-a773659f15a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "won!",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8688627c-3829-4ec7-9f48-c9e54fa51ba7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "725d0487-0e24-4fba-ac42-a773659f15a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "Title",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "7cf89a58-5158-44d6-923d-9c458ecdce0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "725d0487-0e24-4fba-ac42-a773659f15a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1980,
    "xorig": 0,
    "yorig": 0
}