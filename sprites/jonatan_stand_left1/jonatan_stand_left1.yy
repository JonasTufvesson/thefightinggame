{
    "id": "484fd21f-0e35-40cf-99d8-dee9f00da29a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_left1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e84820bd-430d-4b72-afcd-558227e13af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "484fd21f-0e35-40cf-99d8-dee9f00da29a",
            "compositeImage": {
                "id": "dd9350b8-330f-4d08-9147-5fd8c701b071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e84820bd-430d-4b72-afcd-558227e13af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcb5602b-f0e6-491a-abd9-65ce8c0162b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e84820bd-430d-4b72-afcd-558227e13af2",
                    "LayerId": "7bacd939-18ee-4da0-a1ff-b59a5e10c734"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "7bacd939-18ee-4da0-a1ff-b59a5e10c734",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "484fd21f-0e35-40cf-99d8-dee9f00da29a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 0,
    "yorig": 0
}