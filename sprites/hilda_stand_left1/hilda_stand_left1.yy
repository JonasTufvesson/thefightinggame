{
    "id": "bce96f9c-5738-4930-b5e9-de290daadc1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_stand_left1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 3,
    "bbox_right": 50,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f240079-caa4-40a5-8a2d-b5b705872d3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bce96f9c-5738-4930-b5e9-de290daadc1e",
            "compositeImage": {
                "id": "e330c5c5-f64b-4d7c-b363-2d45b3d61608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f240079-caa4-40a5-8a2d-b5b705872d3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61747ed4-3652-4af4-8a70-a19956d9af24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f240079-caa4-40a5-8a2d-b5b705872d3f",
                    "LayerId": "a9815372-8133-4b96-a67a-dc72a241f05d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "a9815372-8133-4b96-a67a-dc72a241f05d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bce96f9c-5738-4930-b5e9-de290daadc1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}