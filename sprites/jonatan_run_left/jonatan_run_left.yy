{
    "id": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_run_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 2,
    "bbox_right": 49,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64dbc9b7-e5e8-4f5b-8b19-fe3742b8746b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
            "compositeImage": {
                "id": "bce35740-13f2-4cee-9b6e-c47650d48698",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64dbc9b7-e5e8-4f5b-8b19-fe3742b8746b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52ab43aa-edba-434a-8671-33ae2ac89c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64dbc9b7-e5e8-4f5b-8b19-fe3742b8746b",
                    "LayerId": "a0ba0805-53fa-4168-896e-4f93d1a060ce"
                }
            ]
        },
        {
            "id": "fb47c38c-6b48-45ad-84cc-c969af0aaff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
            "compositeImage": {
                "id": "62b8af92-f67f-489e-b40e-ee620201d434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb47c38c-6b48-45ad-84cc-c969af0aaff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cab69733-8b5f-4198-84c7-fd8d9b9c458c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb47c38c-6b48-45ad-84cc-c969af0aaff6",
                    "LayerId": "a0ba0805-53fa-4168-896e-4f93d1a060ce"
                }
            ]
        },
        {
            "id": "14fdb6af-ae8b-433d-93c5-b968a2a9bb5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
            "compositeImage": {
                "id": "813f4c78-c86a-4973-acc7-9e39b65e7013",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14fdb6af-ae8b-433d-93c5-b968a2a9bb5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9914e3b8-d304-46b1-8bb6-2e5ca6922b49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14fdb6af-ae8b-433d-93c5-b968a2a9bb5a",
                    "LayerId": "a0ba0805-53fa-4168-896e-4f93d1a060ce"
                }
            ]
        },
        {
            "id": "dfb0fdd8-08fc-4ecd-aee1-e24221130c88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
            "compositeImage": {
                "id": "4d134009-4719-4321-a0f7-8b83715e8a5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb0fdd8-08fc-4ecd-aee1-e24221130c88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eac48877-ef13-47c6-b4c7-1822829eeb00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb0fdd8-08fc-4ecd-aee1-e24221130c88",
                    "LayerId": "a0ba0805-53fa-4168-896e-4f93d1a060ce"
                }
            ]
        },
        {
            "id": "19915e40-543c-4e34-a75c-5bca2ce9c6fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
            "compositeImage": {
                "id": "6f507474-e3c3-44a3-ab1b-cfb16792a309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19915e40-543c-4e34-a75c-5bca2ce9c6fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ba137f9-8ca7-481b-aada-e1039acbb16c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19915e40-543c-4e34-a75c-5bca2ce9c6fe",
                    "LayerId": "a0ba0805-53fa-4168-896e-4f93d1a060ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "a0ba0805-53fa-4168-896e-4f93d1a060ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f4df049-4286-4f59-a4ff-7fcbba64d26f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}