{
    "id": "dd79fe16-e792-4858-9a24-0b0e0131e004",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_stand_right1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 14,
    "bbox_right": 61,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "49b666d5-95d5-4fa1-b2a3-603e74a165f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd79fe16-e792-4858-9a24-0b0e0131e004",
            "compositeImage": {
                "id": "c02448d8-2df0-42f0-95e7-bcb9e38bd855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b666d5-95d5-4fa1-b2a3-603e74a165f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e37e4edf-9183-474b-8c43-36462ba93b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b666d5-95d5-4fa1-b2a3-603e74a165f5",
                    "LayerId": "26ed73ab-939d-4498-b5e3-f289af172609"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "26ed73ab-939d-4498-b5e3-f289af172609",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd79fe16-e792-4858-9a24-0b0e0131e004",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}