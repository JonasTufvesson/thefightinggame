{
    "id": "37ab9ece-d537-41e4-b51a-54bcfdc4812d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 4,
    "bbox_right": 49,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b98fd0d3-ffe3-4754-b1a5-bdedeece1830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37ab9ece-d537-41e4-b51a-54bcfdc4812d",
            "compositeImage": {
                "id": "4db0b25a-f46e-469f-a562-3e3df600c823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b98fd0d3-ffe3-4754-b1a5-bdedeece1830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7aed5f-0b55-42d0-930d-865d236b4d84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b98fd0d3-ffe3-4754-b1a5-bdedeece1830",
                    "LayerId": "169f141e-7103-4793-b258-9f7d1b248ac3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "169f141e-7103-4793-b258-9f7d1b248ac3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37ab9ece-d537-41e4-b51a-54bcfdc4812d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}