{
    "id": "13998c4d-d7d3-4925-a78e-24bffaf4c278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_kick_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 164,
    "bbox_left": 6,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ec214b3-ad47-44ae-80fb-c8ea6b2d8a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "13998c4d-d7d3-4925-a78e-24bffaf4c278",
            "compositeImage": {
                "id": "087be9b1-6f1e-4ce5-91ad-b267bb49d2ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec214b3-ad47-44ae-80fb-c8ea6b2d8a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13a86f9d-564c-4c40-88fc-d8fb46bc0bcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec214b3-ad47-44ae-80fb-c8ea6b2d8a86",
                    "LayerId": "a4346c8d-8cc7-4255-bd5b-b5899c8254dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "a4346c8d-8cc7-4255-bd5b-b5899c8254dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "13998c4d-d7d3-4925-a78e-24bffaf4c278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 0,
    "yorig": 0
}