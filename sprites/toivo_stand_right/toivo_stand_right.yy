{
    "id": "c7bc5ded-327c-4a0a-9bc3-4028bb589754",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_stand_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 11,
    "bbox_right": 55,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87e8cdd2-3a6e-47f1-846a-3d1a2f51322f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7bc5ded-327c-4a0a-9bc3-4028bb589754",
            "compositeImage": {
                "id": "2688399a-97b5-44f5-8345-53a091f8120b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87e8cdd2-3a6e-47f1-846a-3d1a2f51322f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf44ffbe-6932-41f2-8546-b07f19dff699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87e8cdd2-3a6e-47f1-846a-3d1a2f51322f",
                    "LayerId": "808d4171-9c85-4642-b67e-e83d32118869"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "808d4171-9c85-4642-b67e-e83d32118869",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7bc5ded-327c-4a0a-9bc3-4028bb589754",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}