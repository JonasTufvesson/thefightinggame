{
    "id": "521f24db-cd9e-41b5-9287-f653d2b2200c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_kick_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 161,
    "bbox_left": 60,
    "bbox_right": 107,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4965067f-2b9c-4cfc-b915-a2d6c7a41b51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "521f24db-cd9e-41b5-9287-f653d2b2200c",
            "compositeImage": {
                "id": "4924bbd6-a95f-479d-82c4-bd1954d69422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4965067f-2b9c-4cfc-b915-a2d6c7a41b51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846307b5-8018-46a3-858e-def130549597",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4965067f-2b9c-4cfc-b915-a2d6c7a41b51",
                    "LayerId": "2ec76b22-cfef-4f71-9ba3-5d4b335c5738"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "2ec76b22-cfef-4f71-9ba3-5d4b335c5738",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "521f24db-cd9e-41b5-9287-f653d2b2200c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 0
}