{
    "id": "2d635315-0799-422d-bdc7-b1dbc64ad5a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 1,
    "bbox_right": 65,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e7003f1-664c-45b5-b19f-b5a91a7490b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d635315-0799-422d-bdc7-b1dbc64ad5a4",
            "compositeImage": {
                "id": "257533eb-9da4-410f-9318-3c4bbe6bdc75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e7003f1-664c-45b5-b19f-b5a91a7490b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55f824f7-f62e-4afe-b7f7-5d548b906f19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e7003f1-664c-45b5-b19f-b5a91a7490b1",
                    "LayerId": "d922c68e-9e02-4bc1-969c-5054d76a39fd"
                }
            ]
        },
        {
            "id": "f5ca7926-163d-4144-a693-a16eb9f289d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d635315-0799-422d-bdc7-b1dbc64ad5a4",
            "compositeImage": {
                "id": "0e2e1552-a7fc-442f-b646-8cfd85c34f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ca7926-163d-4144-a693-a16eb9f289d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f03d33-b992-4cea-a64f-23f5f79d7593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ca7926-163d-4144-a693-a16eb9f289d9",
                    "LayerId": "d922c68e-9e02-4bc1-969c-5054d76a39fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "d922c68e-9e02-4bc1-969c-5054d76a39fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d635315-0799-422d-bdc7-b1dbc64ad5a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 0,
    "yorig": 0
}