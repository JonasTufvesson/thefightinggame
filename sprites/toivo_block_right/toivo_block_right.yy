{
    "id": "e007f5a1-45a7-4337-b036-9cabc4f55c93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_block_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 3,
    "bbox_right": 49,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa91c995-4356-4521-b40b-bf616e897073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e007f5a1-45a7-4337-b036-9cabc4f55c93",
            "compositeImage": {
                "id": "dcbb13c0-f1b5-42ee-9638-2e24e19f34c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa91c995-4356-4521-b40b-bf616e897073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc9c5e8-44cf-4d9f-914f-2c9beef6ab60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa91c995-4356-4521-b40b-bf616e897073",
                    "LayerId": "38472c22-2610-4ee8-87b4-d823938aa229"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "38472c22-2610-4ee8-87b4-d823938aa229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e007f5a1-45a7-4337-b036-9cabc4f55c93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 0,
    "yorig": 0
}