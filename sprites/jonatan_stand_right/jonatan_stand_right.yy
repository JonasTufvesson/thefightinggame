{
    "id": "879aa995-cb7b-43d6-968f-f8b86d667e17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51118df4-da3e-4385-a8dd-79f74bc10a47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "879aa995-cb7b-43d6-968f-f8b86d667e17",
            "compositeImage": {
                "id": "7a1c02ea-0611-46be-901b-61da95c8abf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51118df4-da3e-4385-a8dd-79f74bc10a47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec42f3d7-4f3d-4316-bfef-2fcd7e37d57e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51118df4-da3e-4385-a8dd-79f74bc10a47",
                    "LayerId": "82dd58e2-562a-4ac0-9214-6bf77095d474"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "82dd58e2-562a-4ac0-9214-6bf77095d474",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "879aa995-cb7b-43d6-968f-f8b86d667e17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}