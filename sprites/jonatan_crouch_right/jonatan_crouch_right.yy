{
    "id": "bbf24a26-1766-4c26-ab6b-08c562b76c31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_crouch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 5,
    "bbox_right": 39,
    "bbox_top": 115,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3beab70e-1702-4d0b-ae87-f25488042974",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bbf24a26-1766-4c26-ab6b-08c562b76c31",
            "compositeImage": {
                "id": "f6c60494-62f0-4564-a302-69d7ab86a534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3beab70e-1702-4d0b-ae87-f25488042974",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1213992-5361-4d05-b962-8c9da0aa7f7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3beab70e-1702-4d0b-ae87-f25488042974",
                    "LayerId": "d065544c-0649-4d4a-9f09-3f28deac73b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "d065544c-0649-4d4a-9f09-3f28deac73b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bbf24a26-1766-4c26-ab6b-08c562b76c31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 0,
    "yorig": 0
}