{
    "id": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4cc6dfe6-32c1-480b-a956-5b3d780e4811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "compositeImage": {
                "id": "63609f9a-049e-4d5a-afe6-7f2d311e97d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cc6dfe6-32c1-480b-a956-5b3d780e4811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e9caa08-840d-4c94-8e09-d0cb615cb082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cc6dfe6-32c1-480b-a956-5b3d780e4811",
                    "LayerId": "b5038ab1-3c6a-414e-a09b-552bafe1c439"
                }
            ]
        },
        {
            "id": "f160fb8f-5144-4769-ae8c-b592e7b68827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "compositeImage": {
                "id": "f05c0cdb-112b-4199-9f3f-6080c2c00fea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f160fb8f-5144-4769-ae8c-b592e7b68827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79ab68df-f539-4398-a463-cd404a214d9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f160fb8f-5144-4769-ae8c-b592e7b68827",
                    "LayerId": "b5038ab1-3c6a-414e-a09b-552bafe1c439"
                }
            ]
        },
        {
            "id": "55c4703a-e8c0-4e63-ac00-f523d5ac656f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "compositeImage": {
                "id": "3391f102-0cfd-4ebf-8825-d5d9a8bc3381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c4703a-e8c0-4e63-ac00-f523d5ac656f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58742f82-b3fc-41f9-8e34-ab761edbe9b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c4703a-e8c0-4e63-ac00-f523d5ac656f",
                    "LayerId": "b5038ab1-3c6a-414e-a09b-552bafe1c439"
                }
            ]
        },
        {
            "id": "0d86d446-6c31-45f0-8459-153aebf003eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "compositeImage": {
                "id": "eba97e9c-b1b6-4642-abe0-4b6b2722a3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d86d446-6c31-45f0-8459-153aebf003eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "450732aa-2399-4981-baad-39267cff7b61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d86d446-6c31-45f0-8459-153aebf003eb",
                    "LayerId": "b5038ab1-3c6a-414e-a09b-552bafe1c439"
                }
            ]
        },
        {
            "id": "638e09c5-c73a-46e9-aa85-c680f5c5c667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "compositeImage": {
                "id": "8fe930ef-d50a-4d70-b080-2cc2fa3b6e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638e09c5-c73a-46e9-aa85-c680f5c5c667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a182a945-bf1e-4c76-aa9b-263ed369cb20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638e09c5-c73a-46e9-aa85-c680f5c5c667",
                    "LayerId": "b5038ab1-3c6a-414e-a09b-552bafe1c439"
                }
            ]
        },
        {
            "id": "9f100114-0732-4907-b748-be83668a4c0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "compositeImage": {
                "id": "0e65674d-9510-46f7-b416-fb67f1245c21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f100114-0732-4907-b748-be83668a4c0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5644475b-ebe0-459f-9f6c-70776d778e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f100114-0732-4907-b748-be83668a4c0c",
                    "LayerId": "b5038ab1-3c6a-414e-a09b-552bafe1c439"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b5038ab1-3c6a-414e-a09b-552bafe1c439",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9c536e6-fdbe-47c0-b544-c5f3b9729328",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}