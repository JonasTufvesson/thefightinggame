{
    "id": "bd8ad73f-a8eb-472d-9dfe-4cc579c27a69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_crouch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "375df4d4-c38a-4842-a785-de47f0f048e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd8ad73f-a8eb-472d-9dfe-4cc579c27a69",
            "compositeImage": {
                "id": "3dc818b1-bd8b-49ad-b25a-30a458264c99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375df4d4-c38a-4842-a785-de47f0f048e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b193174-7a27-45cf-a90d-40599143524f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375df4d4-c38a-4842-a785-de47f0f048e3",
                    "LayerId": "cb0aa605-9077-4990-8dcf-272330f9f050"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "cb0aa605-9077-4990-8dcf-272330f9f050",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd8ad73f-a8eb-472d-9dfe-4cc579c27a69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}