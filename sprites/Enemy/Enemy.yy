{
    "id": "1cd4e8d1-7abb-4d0b-a75e-1c5ce9821345",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 242,
    "bbox_left": 61,
    "bbox_right": 183,
    "bbox_top": 85,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58781375-e0d0-4bb2-a8c2-a6727037fae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cd4e8d1-7abb-4d0b-a75e-1c5ce9821345",
            "compositeImage": {
                "id": "5d31aea0-2a3b-41ee-ac1f-5f575428d05d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58781375-e0d0-4bb2-a8c2-a6727037fae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b878ac66-d04c-444e-945a-dc0617401453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58781375-e0d0-4bb2-a8c2-a6727037fae5",
                    "LayerId": "1ac2101a-6c67-4469-9bc5-8680e25ec51e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1ac2101a-6c67-4469-9bc5-8680e25ec51e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cd4e8d1-7abb-4d0b-a75e-1c5ce9821345",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}