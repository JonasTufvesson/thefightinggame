{
    "id": "57aff0f8-a9f6-43cc-be67-06b57626f952",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_punch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 161,
    "bbox_left": 5,
    "bbox_right": 54,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d7501bb-1476-40d5-89c5-d2b8c63d6188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57aff0f8-a9f6-43cc-be67-06b57626f952",
            "compositeImage": {
                "id": "938f0252-e7be-40d4-8027-8a2edb7b75f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7501bb-1476-40d5-89c5-d2b8c63d6188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bd4f0c5-28b7-4061-9f2a-84cb9e75c906",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7501bb-1476-40d5-89c5-d2b8c63d6188",
                    "LayerId": "237e28ef-e21b-4f18-ab4d-18473385fb2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "237e28ef-e21b-4f18-ab4d-18473385fb2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57aff0f8-a9f6-43cc-be67-06b57626f952",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 0,
    "yorig": 0
}