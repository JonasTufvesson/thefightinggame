{
    "id": "9b9f1630-13cb-4678-8060-d5aa89e0b5a6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_crounch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 126,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a0011ce-879f-44d3-8816-7d8d4bbfa7e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b9f1630-13cb-4678-8060-d5aa89e0b5a6",
            "compositeImage": {
                "id": "0b8df9e3-b9fb-43ae-9f94-5e397ec9a2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a0011ce-879f-44d3-8816-7d8d4bbfa7e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49bc75c-bd93-4dc2-82f3-c53f518f26c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a0011ce-879f-44d3-8816-7d8d4bbfa7e2",
                    "LayerId": "84ded5bd-8b2d-40eb-9068-24656b8bd9e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 159,
    "layers": [
        {
            "id": "84ded5bd-8b2d-40eb-9068-24656b8bd9e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b9f1630-13cb-4678-8060-d5aa89e0b5a6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 0,
    "yorig": 0
}