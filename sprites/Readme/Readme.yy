{
    "id": "39e516d6-a4e5-43a7-b7f1-68b497d62c12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Readme",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb707b3f-8555-45f8-a375-371460f04766",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39e516d6-a4e5-43a7-b7f1-68b497d62c12",
            "compositeImage": {
                "id": "5b67b9df-83a3-4906-8210-f1dd4f0fe209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb707b3f-8555-45f8-a375-371460f04766",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8d1657c-be4d-47e7-9fe3-c5cc8e9e1408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb707b3f-8555-45f8-a375-371460f04766",
                    "LayerId": "2b5e5716-d24f-4e0e-b81c-0800d4d43999"
                },
                {
                    "id": "2653655d-c15a-42e8-a125-d85e1183529b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb707b3f-8555-45f8-a375-371460f04766",
                    "LayerId": "591d8d13-d5b1-464d-95eb-a215e253e298"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "591d8d13-d5b1-464d-95eb-a215e253e298",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39e516d6-a4e5-43a7-b7f1-68b497d62c12",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2b5e5716-d24f-4e0e-b81c-0800d4d43999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39e516d6-a4e5-43a7-b7f1-68b497d62c12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}