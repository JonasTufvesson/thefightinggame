{
    "id": "0c3cc19e-7b16-429d-a2f9-c93727b61785",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_left3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 2,
    "bbox_right": 52,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32c43454-c87e-411f-b731-06e6a256a2c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c3cc19e-7b16-429d-a2f9-c93727b61785",
            "compositeImage": {
                "id": "ea34a762-37d0-4f4a-af70-7f1c292ce838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32c43454-c87e-411f-b731-06e6a256a2c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f1768ac-1dd5-4b88-9cf7-15d55a185d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32c43454-c87e-411f-b731-06e6a256a2c4",
                    "LayerId": "8e99cdb8-4f6a-409c-9fc8-48ec84eee120"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "8e99cdb8-4f6a-409c-9fc8-48ec84eee120",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c3cc19e-7b16-429d-a2f9-c93727b61785",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 0,
    "yorig": 0
}