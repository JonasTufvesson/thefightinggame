{
    "id": "9e0c50e1-c243-4b68-a64d-a73cb06a192d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_punch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 162,
    "bbox_left": 56,
    "bbox_right": 107,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad9bb7ab-216b-4b1f-b7fd-9632abc17d9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e0c50e1-c243-4b68-a64d-a73cb06a192d",
            "compositeImage": {
                "id": "c8ac11c3-d774-407f-8ddf-4deb6d3452de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad9bb7ab-216b-4b1f-b7fd-9632abc17d9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9001ca52-8131-4d9b-a917-c68dff36e357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad9bb7ab-216b-4b1f-b7fd-9632abc17d9a",
                    "LayerId": "6ca7a697-d509-4c04-93c4-2da92f3ab48c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "6ca7a697-d509-4c04-93c4-2da92f3ab48c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e0c50e1-c243-4b68-a64d-a73cb06a192d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 111,
    "xorig": 55,
    "yorig": 0
}