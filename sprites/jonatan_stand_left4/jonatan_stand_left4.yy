{
    "id": "a57ff1af-04e8-470d-9b32-be8b05d58612",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_left4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 8,
    "bbox_right": 56,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f210d299-721b-48cf-a677-eb22516484fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57ff1af-04e8-470d-9b32-be8b05d58612",
            "compositeImage": {
                "id": "78849d0c-a70b-46a2-8c1c-77a2cdcd5c36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f210d299-721b-48cf-a677-eb22516484fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37417595-6c7d-48ae-8f10-6bc00019ae01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f210d299-721b-48cf-a677-eb22516484fe",
                    "LayerId": "d113c0e6-c517-4355-952d-6035f29cb62a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "d113c0e6-c517-4355-952d-6035f29cb62a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a57ff1af-04e8-470d-9b32-be8b05d58612",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 0,
    "yorig": 0
}