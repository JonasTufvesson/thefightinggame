{
    "id": "bc6b5adc-fc3e-4877-9e06-2e4b62ebaf85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_crounch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 115,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e94b9b8-b253-4af2-a29d-de3e5828af3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc6b5adc-fc3e-4877-9e06-2e4b62ebaf85",
            "compositeImage": {
                "id": "49bed3b1-592f-4c86-8dcd-651516c6848c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e94b9b8-b253-4af2-a29d-de3e5828af3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa46d48-2196-47f0-a83c-7e44ed52dfe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e94b9b8-b253-4af2-a29d-de3e5828af3c",
                    "LayerId": "5d234c7a-a675-4c47-b606-5bd812b0a744"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "5d234c7a-a675-4c47-b606-5bd812b0a744",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc6b5adc-fc3e-4877-9e06-2e4b62ebaf85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}