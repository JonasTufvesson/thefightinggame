{
    "id": "50927937-6cdd-41ae-9f58-ba31a43c3d93",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_run_left3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 2,
    "bbox_right": 62,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a8cd724-d138-4820-a870-60f74beaf786",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50927937-6cdd-41ae-9f58-ba31a43c3d93",
            "compositeImage": {
                "id": "9567a540-cc32-47a5-8c64-a46652699f5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8cd724-d138-4820-a870-60f74beaf786",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc959a2-1d31-4d2a-a78f-6e90b398cd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8cd724-d138-4820-a870-60f74beaf786",
                    "LayerId": "9f87d63b-db9a-4a06-adfb-16f10a2da7be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "9f87d63b-db9a-4a06-adfb-16f10a2da7be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50927937-6cdd-41ae-9f58-ba31a43c3d93",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 0,
    "yorig": 0
}