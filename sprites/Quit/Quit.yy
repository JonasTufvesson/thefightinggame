{
    "id": "b697c81c-1ad6-4a9b-b105-9d045da66ee6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Quit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "124b439b-4fc2-4c8f-a9af-7a1f3666b7ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b697c81c-1ad6-4a9b-b105-9d045da66ee6",
            "compositeImage": {
                "id": "86583221-4e2a-46b4-8ebf-46a255fb3241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "124b439b-4fc2-4c8f-a9af-7a1f3666b7ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998d91d9-0075-4173-abe3-0afea312f97f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124b439b-4fc2-4c8f-a9af-7a1f3666b7ac",
                    "LayerId": "d5733250-0823-4f33-bb83-155d704a1e10"
                },
                {
                    "id": "ff1b39a5-4024-493c-9c0d-4556d247e61a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "124b439b-4fc2-4c8f-a9af-7a1f3666b7ac",
                    "LayerId": "59ef5f01-b697-48ca-bc17-6b541f9c351d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "59ef5f01-b697-48ca-bc17-6b541f9c351d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b697c81c-1ad6-4a9b-b105-9d045da66ee6",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d5733250-0823-4f33-bb83-155d704a1e10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b697c81c-1ad6-4a9b-b105-9d045da66ee6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}