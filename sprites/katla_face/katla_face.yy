{
    "id": "7f3b73a3-4c7f-4730-b194-057b97a57eba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_face",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 148,
    "bbox_left": 1,
    "bbox_right": 110,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bca27cae-cd72-4c72-93a2-d0ede544f648",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f3b73a3-4c7f-4730-b194-057b97a57eba",
            "compositeImage": {
                "id": "ea2e5f89-aad1-4f84-83b9-0c89c1bf95a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca27cae-cd72-4c72-93a2-d0ede544f648",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "babcc814-4c5b-42a5-a57a-776b271f3cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca27cae-cd72-4c72-93a2-d0ede544f648",
                    "LayerId": "78a73433-0fe9-4d85-8443-5737088e90e1"
                },
                {
                    "id": "99f54442-11ed-4468-ab0f-befd1d6c1581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca27cae-cd72-4c72-93a2-d0ede544f648",
                    "LayerId": "35c1275b-a711-4a97-8adb-ef815903379d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 151,
    "layers": [
        {
            "id": "35c1275b-a711-4a97-8adb-ef815903379d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f3b73a3-4c7f-4730-b194-057b97a57eba",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "78a73433-0fe9-4d85-8443-5737088e90e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f3b73a3-4c7f-4730-b194-057b97a57eba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 0,
    "yorig": 0
}