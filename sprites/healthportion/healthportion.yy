{
    "id": "f989446a-6ffd-45e2-8097-dff958110348",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "healthportion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 3,
    "bbox_right": 45,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b90243fb-9257-42c5-bd19-11535b247c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f989446a-6ffd-45e2-8097-dff958110348",
            "compositeImage": {
                "id": "caad4663-ebb5-45ef-b42f-6a026972ac6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b90243fb-9257-42c5-bd19-11535b247c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f969f7d-8cd0-47df-991b-fafc3c8bd077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b90243fb-9257-42c5-bd19-11535b247c5c",
                    "LayerId": "da10ba06-6b3a-45ae-9b1c-247ba3d1fbd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "da10ba06-6b3a-45ae-9b1c-247ba3d1fbd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f989446a-6ffd-45e2-8097-dff958110348",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}