{
    "id": "0326f7b2-f28a-4cf2-a3e2-8fd535b4d4a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_kick_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 3,
    "bbox_right": 47,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4aec568-7762-4299-8111-cc1e8b0e7396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0326f7b2-f28a-4cf2-a3e2-8fd535b4d4a8",
            "compositeImage": {
                "id": "ea9b4c56-8a6e-4257-a027-2e96e56e307c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4aec568-7762-4299-8111-cc1e8b0e7396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "080e9829-70e2-4a91-b838-946fa900fc3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4aec568-7762-4299-8111-cc1e8b0e7396",
                    "LayerId": "0ff32b08-bcf2-4bad-884f-81c794440268"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 153,
    "layers": [
        {
            "id": "0ff32b08-bcf2-4bad-884f-81c794440268",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0326f7b2-f28a-4cf2-a3e2-8fd535b4d4a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 123,
    "xorig": 0,
    "yorig": 0
}