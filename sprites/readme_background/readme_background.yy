{
    "id": "5512b2b2-d5aa-463d-8005-964ab3fb75e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "readme_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1074,
    "bbox_left": 2,
    "bbox_right": 1977,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66fa382c-eb85-422e-9ba0-fdb2bc138043",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5512b2b2-d5aa-463d-8005-964ab3fb75e7",
            "compositeImage": {
                "id": "d844e134-abd0-4a9f-878e-6e7e9bc839b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66fa382c-eb85-422e-9ba0-fdb2bc138043",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1b49cd9-7365-4631-8c6d-1374c2c2b1ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66fa382c-eb85-422e-9ba0-fdb2bc138043",
                    "LayerId": "311370c3-d115-4694-90f1-a6d71a6ed5d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "311370c3-d115-4694-90f1-a6d71a6ed5d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5512b2b2-d5aa-463d-8005-964ab3fb75e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1980,
    "xorig": 0,
    "yorig": 0
}