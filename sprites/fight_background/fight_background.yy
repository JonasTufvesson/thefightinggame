{
    "id": "c4c0e0ec-9940-4db6-af2f-645f2a71c151",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fight_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1979,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bae79497-9ac2-490e-b3f9-08cc7f84bb7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c0e0ec-9940-4db6-af2f-645f2a71c151",
            "compositeImage": {
                "id": "fce837ca-680a-4b78-b047-feb830ec7e0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bae79497-9ac2-490e-b3f9-08cc7f84bb7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f02536b9-fb86-42e5-b5ab-a6040248f4c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bae79497-9ac2-490e-b3f9-08cc7f84bb7c",
                    "LayerId": "2a034bd3-8bfd-4be7-8ba7-695c492b0866"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "2a034bd3-8bfd-4be7-8ba7-695c492b0866",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4c0e0ec-9940-4db6-af2f-645f2a71c151",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1980,
    "xorig": 0,
    "yorig": 0
}