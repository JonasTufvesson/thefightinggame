{
    "id": "2abb2726-70ca-49ea-a05a-102579a48c0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Play",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "009c2a17-77dd-42c6-ac67-92f453ca94d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2abb2726-70ca-49ea-a05a-102579a48c0b",
            "compositeImage": {
                "id": "02f7f712-e337-43e8-ba29-e5a116ab0f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "009c2a17-77dd-42c6-ac67-92f453ca94d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a3492c3-a50e-4d36-9162-28d125fd7b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009c2a17-77dd-42c6-ac67-92f453ca94d1",
                    "LayerId": "386dbc8e-ce58-47b2-9cd7-c99d2b5a850b"
                },
                {
                    "id": "aaef7729-44b2-4733-852d-5fca3d058e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "009c2a17-77dd-42c6-ac67-92f453ca94d1",
                    "LayerId": "eab25ced-667b-42e5-96c7-16a05ac4887b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "eab25ced-667b-42e5-96c7-16a05ac4887b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2abb2726-70ca-49ea-a05a-102579a48c0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "386dbc8e-ce58-47b2-9cd7-c99d2b5a850b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2abb2726-70ca-49ea-a05a-102579a48c0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}