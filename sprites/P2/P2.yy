{
    "id": "6c64e428-0da0-4714-ae66-6731642ac576",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "P2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e715326a-bd88-4cba-b867-f7e1ba303e01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c64e428-0da0-4714-ae66-6731642ac576",
            "compositeImage": {
                "id": "efb62870-33c2-414c-b6c5-84e3cf1c200a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e715326a-bd88-4cba-b867-f7e1ba303e01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba1d0683-3606-4d10-ab93-de474dc102bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e715326a-bd88-4cba-b867-f7e1ba303e01",
                    "LayerId": "dc51af20-7d1e-4dd8-a98e-1c67690e4b18"
                },
                {
                    "id": "341f9839-890c-4f3b-b068-8a33eb8f4967",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e715326a-bd88-4cba-b867-f7e1ba303e01",
                    "LayerId": "be59ced1-b3ff-404d-8479-766020a74749"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "be59ced1-b3ff-404d-8479-766020a74749",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c64e428-0da0-4714-ae66-6731642ac576",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "dc51af20-7d1e-4dd8-a98e-1c67690e4b18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c64e428-0da0-4714-ae66-6731642ac576",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}