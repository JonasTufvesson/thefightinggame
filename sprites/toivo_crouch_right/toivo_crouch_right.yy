{
    "id": "5f8443d7-db66-4d73-b9d5-3c8d6ab1bebc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_crouch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 122,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eeb7e370-e6aa-4e74-9667-872884d4c210",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f8443d7-db66-4d73-b9d5-3c8d6ab1bebc",
            "compositeImage": {
                "id": "01adc605-549a-4d6b-bb07-92c521c7cb31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeb7e370-e6aa-4e74-9667-872884d4c210",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf90792-b085-4ba4-b879-09804c2d8711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeb7e370-e6aa-4e74-9667-872884d4c210",
                    "LayerId": "8935c517-0c0a-405a-b700-88c3c9075dad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "8935c517-0c0a-405a-b700-88c3c9075dad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f8443d7-db66-4d73-b9d5-3c8d6ab1bebc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}