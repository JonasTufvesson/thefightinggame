{
    "id": "3926a127-c686-4cb3-b39e-531f62343b9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_left2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 6,
    "bbox_right": 71,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2177eab2-36ce-492f-afe0-51510a5681ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3926a127-c686-4cb3-b39e-531f62343b9b",
            "compositeImage": {
                "id": "f22108aa-8a39-4f44-a345-b033a1009477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2177eab2-36ce-492f-afe0-51510a5681ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d224918a-9c21-4d6a-8512-9b389a117d35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2177eab2-36ce-492f-afe0-51510a5681ce",
                    "LayerId": "65d0e12e-643c-4b88-852b-3bb19d8c2f36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "65d0e12e-643c-4b88-852b-3bb19d8c2f36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3926a127-c686-4cb3-b39e-531f62343b9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 76,
    "xorig": 0,
    "yorig": 0
}