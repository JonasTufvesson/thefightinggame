{
    "id": "bff84be8-d49d-45b8-b47d-9bd18aa2f1a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_right2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 4,
    "bbox_right": 69,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "345dce9e-f91a-4964-ad07-41a4d2184414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bff84be8-d49d-45b8-b47d-9bd18aa2f1a0",
            "compositeImage": {
                "id": "cd9d314e-33d7-4fa4-8f55-879f26a50324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "345dce9e-f91a-4964-ad07-41a4d2184414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b458eac5-3809-4c40-8e60-85fafe607e4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "345dce9e-f91a-4964-ad07-41a4d2184414",
                    "LayerId": "e06cb179-9ce8-42b7-a882-a27f9cb9645e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "e06cb179-9ce8-42b7-a882-a27f9cb9645e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bff84be8-d49d-45b8-b47d-9bd18aa2f1a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 76,
    "xorig": 0,
    "yorig": 0
}