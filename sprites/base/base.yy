{
    "id": "1151c637-258e-490a-a589-7b0a9cada200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "base",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 1979,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d766578e-1b04-4727-ae55-5e63603d06c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1151c637-258e-490a-a589-7b0a9cada200",
            "compositeImage": {
                "id": "e4745cf7-2018-4f88-97ff-59241417d787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d766578e-1b04-4727-ae55-5e63603d06c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be012c24-96dc-47e8-8973-e15e8e9efbc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d766578e-1b04-4727-ae55-5e63603d06c2",
                    "LayerId": "33caf2aa-baa1-45b5-a3b9-61317144e77c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "33caf2aa-baa1-45b5-a3b9-61317144e77c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1151c637-258e-490a-a589-7b0a9cada200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1980,
    "xorig": 0,
    "yorig": 0
}