{
    "id": "2dff4ade-a8e7-4c12-a70c-5f66fcfbade7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_crouch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 126,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ac7b70a-31fe-4897-bf4a-d0364030b09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2dff4ade-a8e7-4c12-a70c-5f66fcfbade7",
            "compositeImage": {
                "id": "94ea640c-c964-4c4d-af56-24e00d1bc2e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac7b70a-31fe-4897-bf4a-d0364030b09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "098768f9-dc23-4838-80c2-d90d97bb903b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac7b70a-31fe-4897-bf4a-d0364030b09d",
                    "LayerId": "97b23bfd-4e5b-48e7-b103-f764ad450d3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 159,
    "layers": [
        {
            "id": "97b23bfd-4e5b-48e7-b103-f764ad450d3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2dff4ade-a8e7-4c12-a70c-5f66fcfbade7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 0,
    "yorig": 0
}