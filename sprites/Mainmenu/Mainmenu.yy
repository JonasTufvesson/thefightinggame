{
    "id": "109c8cfe-12b5-4c5b-9ddd-233db4d85a7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Mainmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "886e3c2b-5c68-4f78-a984-0da0bd223839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "109c8cfe-12b5-4c5b-9ddd-233db4d85a7f",
            "compositeImage": {
                "id": "f01dceba-3bb8-48bd-867c-a1f9808fe145",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886e3c2b-5c68-4f78-a984-0da0bd223839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07d01863-1e53-4780-807d-d187113a5ea1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886e3c2b-5c68-4f78-a984-0da0bd223839",
                    "LayerId": "be1f0177-17c7-488f-b8f9-163d0afaf50f"
                },
                {
                    "id": "ac9abeb9-06af-4ea9-be98-01c05abe23c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886e3c2b-5c68-4f78-a984-0da0bd223839",
                    "LayerId": "f10519f6-f681-4acf-811b-fc969d8ef133"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f10519f6-f681-4acf-811b-fc969d8ef133",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "109c8cfe-12b5-4c5b-9ddd-233db4d85a7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "be1f0177-17c7-488f-b8f9-163d0afaf50f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "109c8cfe-12b5-4c5b-9ddd-233db4d85a7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}