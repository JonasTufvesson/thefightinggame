{
    "id": "83a27e06-7cfe-4b2a-942b-092a40e79733",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_right5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 3,
    "bbox_right": 68,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b35ccf5c-1dc3-4841-91b5-12f871f2a44f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83a27e06-7cfe-4b2a-942b-092a40e79733",
            "compositeImage": {
                "id": "e52ed374-1ee0-435a-bbaf-0f88e36a5e38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b35ccf5c-1dc3-4841-91b5-12f871f2a44f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b35b12cc-38d4-4ef8-820a-fbfe139aa324",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b35ccf5c-1dc3-4841-91b5-12f871f2a44f",
                    "LayerId": "8510b712-beef-4d5f-a3cf-4d7db8efdbde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "8510b712-beef-4d5f-a3cf-4d7db8efdbde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83a27e06-7cfe-4b2a-942b-092a40e79733",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 0,
    "yorig": 0
}