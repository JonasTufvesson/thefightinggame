{
    "id": "70c62ad6-abe3-4ef0-bdb9-a3f3798b9452",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_kick_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 67,
    "bbox_right": 110,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "873612d0-20b7-43ee-858c-11a9446f4934",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70c62ad6-abe3-4ef0-bdb9-a3f3798b9452",
            "compositeImage": {
                "id": "d1044f57-eb24-4c6b-b527-3b59ed0a1579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "873612d0-20b7-43ee-858c-11a9446f4934",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82b1da2c-83f1-4dbc-9ced-4825799076d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "873612d0-20b7-43ee-858c-11a9446f4934",
                    "LayerId": "2eb573c8-6996-4609-8668-05bdec8edd7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "2eb573c8-6996-4609-8668-05bdec8edd7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70c62ad6-abe3-4ef0-bdb9-a3f3798b9452",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 0
}