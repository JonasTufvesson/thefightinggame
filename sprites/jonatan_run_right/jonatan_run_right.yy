{
    "id": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "654a9518-a7d1-4744-9c5d-c072edb894db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
            "compositeImage": {
                "id": "72f0668d-d5be-433f-aa98-4cd847a6dba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "654a9518-a7d1-4744-9c5d-c072edb894db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b683caf-1af8-4349-9816-1b46cc138ea3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "654a9518-a7d1-4744-9c5d-c072edb894db",
                    "LayerId": "0e923981-6a3e-48f4-b817-477bc9d2055c"
                }
            ]
        },
        {
            "id": "6ffda7ee-087e-4456-a450-8b4fc246583a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
            "compositeImage": {
                "id": "a4d38998-ed87-47b7-b168-a4f2b2aa1230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ffda7ee-087e-4456-a450-8b4fc246583a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2dd85eb-d684-48b1-94c5-cf2afc5be5c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ffda7ee-087e-4456-a450-8b4fc246583a",
                    "LayerId": "0e923981-6a3e-48f4-b817-477bc9d2055c"
                }
            ]
        },
        {
            "id": "10710877-f7b6-4b9e-9cac-3b19dd405abb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
            "compositeImage": {
                "id": "52a64e67-6c18-4ad8-9e30-8d294e301b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10710877-f7b6-4b9e-9cac-3b19dd405abb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa566aab-5355-409b-978b-7f7790f8a053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10710877-f7b6-4b9e-9cac-3b19dd405abb",
                    "LayerId": "0e923981-6a3e-48f4-b817-477bc9d2055c"
                }
            ]
        },
        {
            "id": "b80ae939-e29b-4ce1-a881-bd1dfe08aa61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
            "compositeImage": {
                "id": "4085b028-6445-4a1f-970a-bbae5616d80b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b80ae939-e29b-4ce1-a881-bd1dfe08aa61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4775e8e6-d23a-4736-83ca-9d9d87a14aa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80ae939-e29b-4ce1-a881-bd1dfe08aa61",
                    "LayerId": "0e923981-6a3e-48f4-b817-477bc9d2055c"
                }
            ]
        },
        {
            "id": "74826ef4-8e90-411c-ace3-424e35dd9b0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
            "compositeImage": {
                "id": "d2ff36ac-f958-4f98-b672-f0c3f830efc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74826ef4-8e90-411c-ace3-424e35dd9b0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8a39a4d-7bef-41f2-b893-237a0b90d8f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74826ef4-8e90-411c-ace3-424e35dd9b0f",
                    "LayerId": "0e923981-6a3e-48f4-b817-477bc9d2055c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "0e923981-6a3e-48f4-b817-477bc9d2055c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36d151b7-7cea-4ddf-94fd-d2d81f677429",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 0,
    "yorig": 0
}