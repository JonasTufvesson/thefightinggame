{
    "id": "57756b8e-76d5-4bcb-b9f1-a9aaa41717c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "P1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 10,
    "bbox_right": 116,
    "bbox_top": 40,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76328f2c-f9a8-4e26-a679-ea32124193b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57756b8e-76d5-4bcb-b9f1-a9aaa41717c9",
            "compositeImage": {
                "id": "d5814350-1676-48cc-969a-083f6b0b4c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76328f2c-f9a8-4e26-a679-ea32124193b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9ca997-36e2-4b95-a3be-7b3a8e5d60bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76328f2c-f9a8-4e26-a679-ea32124193b8",
                    "LayerId": "e611affc-4f40-4986-843c-43f7ab4d91fe"
                },
                {
                    "id": "25967eac-6cf5-454b-9cfd-fb8a5a2d0a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76328f2c-f9a8-4e26-a679-ea32124193b8",
                    "LayerId": "07abbbea-525a-4259-93e1-e7c5a865b2de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "07abbbea-525a-4259-93e1-e7c5a865b2de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57756b8e-76d5-4bcb-b9f1-a9aaa41717c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e611affc-4f40-4986-843c-43f7ab4d91fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57756b8e-76d5-4bcb-b9f1-a9aaa41717c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}