{
    "id": "95ca7d98-198c-49eb-bbcf-19fe0a3aaff0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_face",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 137,
    "bbox_left": 1,
    "bbox_right": 94,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0982c54e-02bf-4285-b71b-d7784831f53f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95ca7d98-198c-49eb-bbcf-19fe0a3aaff0",
            "compositeImage": {
                "id": "1f6d5346-56ea-4d36-861b-5c5a691553ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0982c54e-02bf-4285-b71b-d7784831f53f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8650cd8-fcf9-441a-9534-115c379ced7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0982c54e-02bf-4285-b71b-d7784831f53f",
                    "LayerId": "d7e4ac7f-6fd2-4552-b24d-8d2176db442a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 140,
    "layers": [
        {
            "id": "d7e4ac7f-6fd2-4552-b24d-8d2176db442a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95ca7d98-198c-49eb-bbcf-19fe0a3aaff0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}