{
    "id": "5b6604fd-71c0-4be9-976a-1875c3322aaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_stand_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 46,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd185e2f-0d38-47f8-a13b-4cbc2b63979b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b6604fd-71c0-4be9-976a-1875c3322aaf",
            "compositeImage": {
                "id": "f69ca569-264f-4c9d-9db0-576b967a5828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd185e2f-0d38-47f8-a13b-4cbc2b63979b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad7045b-47b4-499e-a662-11223037608c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd185e2f-0d38-47f8-a13b-4cbc2b63979b",
                    "LayerId": "c51e86d6-a6f6-436e-b8a5-72994fae40d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "c51e86d6-a6f6-436e-b8a5-72994fae40d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b6604fd-71c0-4be9-976a-1875c3322aaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}