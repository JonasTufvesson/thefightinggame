{
    "id": "c0e28529-78f9-4cdb-a299-2bdaa2962360",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_punch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 2,
    "bbox_right": 52,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b751a684-7a96-4daa-9f3e-5062ec9eb09a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0e28529-78f9-4cdb-a299-2bdaa2962360",
            "compositeImage": {
                "id": "20903be4-e91b-4a05-9550-c99ee65f701f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b751a684-7a96-4daa-9f3e-5062ec9eb09a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa9ff7f8-d917-47be-8600-f223375fb1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b751a684-7a96-4daa-9f3e-5062ec9eb09a",
                    "LayerId": "c196044b-a10d-4752-a58c-af5813a80e7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "c196044b-a10d-4752-a58c-af5813a80e7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0e28529-78f9-4cdb-a299-2bdaa2962360",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 0,
    "yorig": 0
}