{
    "id": "3c9e35c1-f7fd-4fde-8e31-2a15f7cc1a9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite88",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "774b1398-a569-4f73-83ba-08d7426e2ebb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c9e35c1-f7fd-4fde-8e31-2a15f7cc1a9f",
            "compositeImage": {
                "id": "2cd3b8e1-4236-4148-9706-270363564ad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "774b1398-a569-4f73-83ba-08d7426e2ebb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "908519d0-51d2-44f2-866a-1404c9b78fc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "774b1398-a569-4f73-83ba-08d7426e2ebb",
                    "LayerId": "f13fd0a1-76bf-4037-8f6f-b1a1485b52d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f13fd0a1-76bf-4037-8f6f-b1a1485b52d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c9e35c1-f7fd-4fde-8e31-2a15f7cc1a9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}