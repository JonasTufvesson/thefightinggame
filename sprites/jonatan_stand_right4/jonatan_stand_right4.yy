{
    "id": "d30ca692-9e91-497a-b6c6-1f71847f2488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_right4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cf6681cc-afb6-45f5-90e5-a3731e68c9b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d30ca692-9e91-497a-b6c6-1f71847f2488",
            "compositeImage": {
                "id": "3a8fa999-5023-4420-86dc-3aa831fc4afe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf6681cc-afb6-45f5-90e5-a3731e68c9b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56417515-0208-48d0-844a-1a011e072f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf6681cc-afb6-45f5-90e5-a3731e68c9b3",
                    "LayerId": "a9169f7f-2ab6-4ffc-a218-cd432c306bc6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "a9169f7f-2ab6-4ffc-a218-cd432c306bc6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d30ca692-9e91-497a-b6c6-1f71847f2488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 0,
    "yorig": 0
}