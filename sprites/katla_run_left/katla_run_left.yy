{
    "id": "5ac52769-53aa-46f8-9788-ff875d14432b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_run_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 20,
    "bbox_right": 78,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a743baa7-fb8a-4c56-919c-db8f28af1414",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ac52769-53aa-46f8-9788-ff875d14432b",
            "compositeImage": {
                "id": "adbd261b-9e6b-4bd2-b4b2-8cc99077be3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a743baa7-fb8a-4c56-919c-db8f28af1414",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb01433-228b-4131-94d8-b433117d6525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a743baa7-fb8a-4c56-919c-db8f28af1414",
                    "LayerId": "2737026c-f63f-4779-8895-bf7d4a906b3d"
                },
                {
                    "id": "4971bcdc-3c68-47d3-a26d-8ba8abd85c5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a743baa7-fb8a-4c56-919c-db8f28af1414",
                    "LayerId": "433d9867-316a-4e29-8684-7e6a17417dc8"
                }
            ]
        },
        {
            "id": "da61d99e-7c50-43a6-80ee-ec3a007bbd6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ac52769-53aa-46f8-9788-ff875d14432b",
            "compositeImage": {
                "id": "427d5e0d-cde2-4181-8e5c-e3eb6525d13d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da61d99e-7c50-43a6-80ee-ec3a007bbd6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23ae2f32-cfc7-4a8b-94d5-a09aef03bb2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da61d99e-7c50-43a6-80ee-ec3a007bbd6c",
                    "LayerId": "2737026c-f63f-4779-8895-bf7d4a906b3d"
                },
                {
                    "id": "bf378408-8578-4ab9-bc4a-38de6d0c2dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da61d99e-7c50-43a6-80ee-ec3a007bbd6c",
                    "LayerId": "433d9867-316a-4e29-8684-7e6a17417dc8"
                }
            ]
        },
        {
            "id": "23662274-72c9-43b7-b0e8-efd38bf88be4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ac52769-53aa-46f8-9788-ff875d14432b",
            "compositeImage": {
                "id": "868acca6-b587-45b0-8643-c6c260ab546f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23662274-72c9-43b7-b0e8-efd38bf88be4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7364348e-86ff-4cfe-a403-81623be331f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23662274-72c9-43b7-b0e8-efd38bf88be4",
                    "LayerId": "2737026c-f63f-4779-8895-bf7d4a906b3d"
                },
                {
                    "id": "dfa01eb5-35a2-4b9e-b90c-2afbf52d00ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23662274-72c9-43b7-b0e8-efd38bf88be4",
                    "LayerId": "433d9867-316a-4e29-8684-7e6a17417dc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "2737026c-f63f-4779-8895-bf7d4a906b3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ac52769-53aa-46f8-9788-ff875d14432b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "433d9867-316a-4e29-8684-7e6a17417dc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ac52769-53aa-46f8-9788-ff875d14432b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 0
}