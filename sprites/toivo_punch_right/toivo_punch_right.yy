{
    "id": "e66dacf3-27c0-4aef-987c-65a0613dbbd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_punch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 161,
    "bbox_left": 45,
    "bbox_right": 93,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b67e933-301d-4acd-a7cf-4b152a196a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e66dacf3-27c0-4aef-987c-65a0613dbbd3",
            "compositeImage": {
                "id": "634246ca-4a03-4469-9771-cea9b72b5f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b67e933-301d-4acd-a7cf-4b152a196a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba04ef43-b0a0-45fc-9260-073e99fa0bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b67e933-301d-4acd-a7cf-4b152a196a94",
                    "LayerId": "e3ce2dc8-4850-4b1a-9889-b483aa33d2ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "e3ce2dc8-4850-4b1a-9889-b483aa33d2ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e66dacf3-27c0-4aef-987c-65a0613dbbd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 49,
    "yorig": 0
}