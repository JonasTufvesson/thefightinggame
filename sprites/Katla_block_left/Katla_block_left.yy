{
    "id": "b166c860-b532-440c-a9a9-ad484d22e028",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Katla_block_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bddfaa58-cd6e-48f1-8ff2-23855040b5b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b166c860-b532-440c-a9a9-ad484d22e028",
            "compositeImage": {
                "id": "05fd6485-ef3c-4696-ba4e-396af473e04e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bddfaa58-cd6e-48f1-8ff2-23855040b5b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b669a2-af6a-434f-bfba-81ad966123ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bddfaa58-cd6e-48f1-8ff2-23855040b5b7",
                    "LayerId": "9e7d0607-e03c-4de3-9b72-e794f4ca0172"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "9e7d0607-e03c-4de3-9b72-e794f4ca0172",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b166c860-b532-440c-a9a9-ad484d22e028",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 0,
    "yorig": 0
}