{
    "id": "b0ced354-456a-4ece-ac1d-4a674d093f52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_punch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 162,
    "bbox_left": 9,
    "bbox_right": 67,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dec47973-6d46-4ef4-95ac-9dde6d9b22e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0ced354-456a-4ece-ac1d-4a674d093f52",
            "compositeImage": {
                "id": "f336a6c1-614b-4cb2-9da1-ee36270c40fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec47973-6d46-4ef4-95ac-9dde6d9b22e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e69e811-498e-4e85-8a8d-b9ca87a09d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec47973-6d46-4ef4-95ac-9dde6d9b22e2",
                    "LayerId": "de31e91f-eec2-452d-abdb-b2c9d77bdf08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "de31e91f-eec2-452d-abdb-b2c9d77bdf08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0ced354-456a-4ece-ac1d-4a674d093f52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 117,
    "xorig": 0,
    "yorig": 0
}