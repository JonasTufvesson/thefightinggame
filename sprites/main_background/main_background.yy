{
    "id": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "main_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1113,
    "bbox_left": 0,
    "bbox_right": 1979,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "compositeImage": {
                "id": "86d631da-683b-4540-8781-a1513e5f4da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132c264d-2629-47e5-afb5-af03da1c53f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "91f78f60-2130-40d9-b128-09d09e68f270"
                },
                {
                    "id": "9b07c356-bc01-4e71-91a4-630c4beb610b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "e4679f4c-4d90-4a5d-b0a3-1b0099569223"
                },
                {
                    "id": "092c51e0-ed45-4042-804e-e3db3e72a74c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "08607f9d-a1f1-4e74-979d-a3e84aacc6f1"
                },
                {
                    "id": "ed424974-887b-4aa7-b6bc-dfcfab083728",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "b3c349b4-4474-4753-b722-410432946b8d"
                },
                {
                    "id": "2edeb7f4-9a2b-46fd-877f-353bd04dd619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "ab7dd2b7-7dae-43c9-a364-6aa3726f26ff"
                },
                {
                    "id": "8c5680de-e2a0-4600-a7ae-150597d41d54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "d7dbe398-bd8a-4bb1-9278-289a692b97cb"
                },
                {
                    "id": "08ca8433-b10a-4a28-8a88-d345903e3532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "c4733590-c07c-41b1-a941-c3f209eded5f"
                },
                {
                    "id": "1c6b477d-65c5-4814-9f76-259b1f8f0c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70246516-9eb0-49f7-88f5-fd3aac0517ce",
                    "LayerId": "6f856ad5-5ce7-4668-b49a-3ba4c48156a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1114,
    "layers": [
        {
            "id": "b3c349b4-4474-4753-b722-410432946b8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Player 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "c4733590-c07c-41b1-a941-c3f209eded5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Jumper2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d7dbe398-bd8a-4bb1-9278-289a692b97cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Jumper1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "6f856ad5-5ce7-4668-b49a-3ba4c48156a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Runner1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "ab7dd2b7-7dae-43c9-a364-6aa3726f26ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Runner2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "08607f9d-a1f1-4e74-979d-a3e84aacc6f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Player 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "e4679f4c-4d90-4a5d-b0a3-1b0099569223",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "Title",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "91f78f60-2130-40d9-b128-09d09e68f270",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec57c0e6-4d6a-438c-8de9-cd5cfcc4fe00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1980,
    "xorig": 0,
    "yorig": 0
}