{
    "id": "135aeefc-464f-447f-804c-21f3e390d494",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_punch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3eea7001-4ba7-4392-ae7c-7ba4b8259522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "135aeefc-464f-447f-804c-21f3e390d494",
            "compositeImage": {
                "id": "c45f57f6-94a2-4cd1-9d89-bd9e2a14a4e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eea7001-4ba7-4392-ae7c-7ba4b8259522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0374c6-33c5-4084-a764-a2d490387542",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eea7001-4ba7-4392-ae7c-7ba4b8259522",
                    "LayerId": "c4cd05be-b561-402e-bce5-973d67776d59"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "c4cd05be-b561-402e-bce5-973d67776d59",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "135aeefc-464f-447f-804c-21f3e390d494",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 93,
    "xorig": 0,
    "yorig": 0
}