{
    "id": "f64c674c-3b83-48a8-822e-77243bbe9010",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_kick_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 4,
    "bbox_right": 49,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab1f8af6-e900-440c-896e-6e4fb104f6f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f64c674c-3b83-48a8-822e-77243bbe9010",
            "compositeImage": {
                "id": "5139f9c8-1dff-43e4-bb81-5324f297715c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab1f8af6-e900-440c-896e-6e4fb104f6f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88409c98-ee85-402e-be3f-25d40db5a86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab1f8af6-e900-440c-896e-6e4fb104f6f6",
                    "LayerId": "6f715f14-95de-4ef0-90b9-fc1134fb4740"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 159,
    "layers": [
        {
            "id": "6f715f14-95de-4ef0-90b9-fc1134fb4740",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f64c674c-3b83-48a8-822e-77243bbe9010",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 0,
    "yorig": 0
}