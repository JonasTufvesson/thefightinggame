{
    "id": "820d61a1-c4d3-4853-83c8-d1d97c7debaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_kick_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 79,
    "bbox_right": 123,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2aea90a-2da3-4d79-a145-4aac4b305307",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "820d61a1-c4d3-4853-83c8-d1d97c7debaf",
            "compositeImage": {
                "id": "6a5a67ca-e066-47f2-b871-6683a088e665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2aea90a-2da3-4d79-a145-4aac4b305307",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6baaa0f-3419-4299-aafb-bd572ce16731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2aea90a-2da3-4d79-a145-4aac4b305307",
                    "LayerId": "0ffaa5f6-9d32-454b-a98e-9d5bed069f60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "0ffaa5f6-9d32-454b-a98e-9d5bed069f60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "820d61a1-c4d3-4853-83c8-d1d97c7debaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 130,
    "xorig": 65,
    "yorig": 0
}