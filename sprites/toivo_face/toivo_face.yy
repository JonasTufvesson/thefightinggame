{
    "id": "ec33a169-be3f-4c84-8416-833449c9baee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_face",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 131,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26641946-53b1-4506-83e0-cb48b4e5ea4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec33a169-be3f-4c84-8416-833449c9baee",
            "compositeImage": {
                "id": "51dff821-bc50-4df5-bc9f-e0114f5ef5ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26641946-53b1-4506-83e0-cb48b4e5ea4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6987fb-656c-4c37-a70a-88ca46c7c834",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26641946-53b1-4506-83e0-cb48b4e5ea4e",
                    "LayerId": "99dd8438-71fd-4a04-838e-25d269573f99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "99dd8438-71fd-4a04-838e-25d269573f99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec33a169-be3f-4c84-8416-833449c9baee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 0,
    "yorig": 0
}