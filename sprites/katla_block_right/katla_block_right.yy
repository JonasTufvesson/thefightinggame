{
    "id": "f1544912-ac33-4682-a35f-fb468284aedc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_block_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f76e80ed-67d1-4acc-8bf2-eea91ec30f1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1544912-ac33-4682-a35f-fb468284aedc",
            "compositeImage": {
                "id": "d868ad0e-93bd-4735-aaf0-c6eff021d41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76e80ed-67d1-4acc-8bf2-eea91ec30f1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42e0ef25-624d-4903-b0dc-31a4fd3ec210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76e80ed-67d1-4acc-8bf2-eea91ec30f1f",
                    "LayerId": "8c300743-efb5-4515-a33a-46e9e548b2de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "8c300743-efb5-4515-a33a-46e9e548b2de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1544912-ac33-4682-a35f-fb468284aedc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 0,
    "yorig": 0
}