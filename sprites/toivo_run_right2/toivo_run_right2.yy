{
    "id": "a50320f6-4dbc-4997-89fe-0c47865467ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_run_right2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 1,
    "bbox_right": 64,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbd47a6d-dbbe-4859-a6f2-213d7e11e16d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a50320f6-4dbc-4997-89fe-0c47865467ed",
            "compositeImage": {
                "id": "47314746-1ab7-449e-aab8-f45e1bba8cdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd47a6d-dbbe-4859-a6f2-213d7e11e16d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40b6fe0-75ea-4493-8078-1b2704aefcf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd47a6d-dbbe-4859-a6f2-213d7e11e16d",
                    "LayerId": "07f86959-6215-457a-b3c6-8fbd122c0dc4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "07f86959-6215-457a-b3c6-8fbd122c0dc4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a50320f6-4dbc-4997-89fe-0c47865467ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 0,
    "yorig": 0
}