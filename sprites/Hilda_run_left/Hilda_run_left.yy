{
    "id": "0e9db9a5-c6da-4af6-a46d-cf9dfe61ac15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Hilda_run_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 3,
    "bbox_right": 53,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b57779c6-b214-4153-8f1b-b41c3cc3f6e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9db9a5-c6da-4af6-a46d-cf9dfe61ac15",
            "compositeImage": {
                "id": "499ea1dd-7ea3-425e-8421-ad34b16aa618",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b57779c6-b214-4153-8f1b-b41c3cc3f6e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "463b70c2-74cd-4358-8b80-987f0d186ed4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b57779c6-b214-4153-8f1b-b41c3cc3f6e5",
                    "LayerId": "5848bc6c-c6da-465a-98f8-d9b8f36ba00e"
                }
            ]
        },
        {
            "id": "f45fdf68-e4a0-48c6-baef-4f1ae4697457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9db9a5-c6da-4af6-a46d-cf9dfe61ac15",
            "compositeImage": {
                "id": "32fd0167-8333-4a2d-9b65-4c0f852b34a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45fdf68-e4a0-48c6-baef-4f1ae4697457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f10071e2-d4bd-417d-9b4b-fc47618c986a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45fdf68-e4a0-48c6-baef-4f1ae4697457",
                    "LayerId": "5848bc6c-c6da-465a-98f8-d9b8f36ba00e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "5848bc6c-c6da-465a-98f8-d9b8f36ba00e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e9db9a5-c6da-4af6-a46d-cf9dfe61ac15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}