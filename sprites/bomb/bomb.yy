{
    "id": "53b24e0b-ea4f-4491-875c-801cb3eaf074",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bomb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97d7c49d-47c1-4322-adf5-f6714ca6ac35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53b24e0b-ea4f-4491-875c-801cb3eaf074",
            "compositeImage": {
                "id": "f018a33f-e6bd-467e-9509-6ef5728d8a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97d7c49d-47c1-4322-adf5-f6714ca6ac35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "449d0bd7-361d-4631-bdde-7ae3ae61bbcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97d7c49d-47c1-4322-adf5-f6714ca6ac35",
                    "LayerId": "71e04dcc-a36a-4bab-a16e-ac7ee4798e33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "71e04dcc-a36a-4bab-a16e-ac7ee4798e33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53b24e0b-ea4f-4491-875c-801cb3eaf074",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 0,
    "yorig": 0
}