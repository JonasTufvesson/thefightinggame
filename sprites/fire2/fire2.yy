{
    "id": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fire2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 64,
    "bbox_right": 195,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "877b943d-2d5f-45e7-931b-e62f0102729c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
            "compositeImage": {
                "id": "d62f293f-b591-4f86-bc8d-8abbfa9656fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877b943d-2d5f-45e7-931b-e62f0102729c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d19b30d-ee2c-4373-97b8-ac6f85c1451b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877b943d-2d5f-45e7-931b-e62f0102729c",
                    "LayerId": "f4653fcb-1b6b-4675-ae0e-c2672bd39fa6"
                }
            ]
        },
        {
            "id": "b018318c-473e-440c-ad5e-789512ab0200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
            "compositeImage": {
                "id": "f905a741-dbfa-46bc-b5ab-0cad66810792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b018318c-473e-440c-ad5e-789512ab0200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0618e204-5f9d-432d-b601-fbd5507a2f6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b018318c-473e-440c-ad5e-789512ab0200",
                    "LayerId": "f4653fcb-1b6b-4675-ae0e-c2672bd39fa6"
                }
            ]
        },
        {
            "id": "6ec2646a-adee-440e-a06e-1d4f97b3b501",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
            "compositeImage": {
                "id": "c99ab023-d43f-4261-91ae-497cc5595cdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ec2646a-adee-440e-a06e-1d4f97b3b501",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693783a4-e919-4312-9e82-72016043c7a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ec2646a-adee-440e-a06e-1d4f97b3b501",
                    "LayerId": "f4653fcb-1b6b-4675-ae0e-c2672bd39fa6"
                }
            ]
        },
        {
            "id": "a01717a0-5b6f-47e0-9b49-5a4c085ac7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
            "compositeImage": {
                "id": "9302700b-09c6-4fe1-874c-362f28a9ab82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a01717a0-5b6f-47e0-9b49-5a4c085ac7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d667f3c8-8595-4000-82a3-907c4cc2ce77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a01717a0-5b6f-47e0-9b49-5a4c085ac7e8",
                    "LayerId": "f4653fcb-1b6b-4675-ae0e-c2672bd39fa6"
                }
            ]
        },
        {
            "id": "f9c63feb-e042-41a7-9061-9e3ab115a120",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
            "compositeImage": {
                "id": "fef0b213-eca0-4124-b7a7-dda0d5432911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c63feb-e042-41a7-9061-9e3ab115a120",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e82a3825-089c-4550-b99c-bd8b6e7716fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c63feb-e042-41a7-9061-9e3ab115a120",
                    "LayerId": "f4653fcb-1b6b-4675-ae0e-c2672bd39fa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f4653fcb-1b6b-4675-ae0e-c2672bd39fa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7eaa69e-7260-4105-9566-e6c11bc52cfb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}