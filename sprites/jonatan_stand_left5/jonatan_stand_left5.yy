{
    "id": "4f001ab6-c951-4a63-815d-49c7fa2607c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_left5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 4,
    "bbox_right": 69,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3bf7b4e6-c72a-442d-9fc7-fea31c276188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f001ab6-c951-4a63-815d-49c7fa2607c2",
            "compositeImage": {
                "id": "519f82ab-d75d-4fa6-8f06-73549dadd0e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf7b4e6-c72a-442d-9fc7-fea31c276188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d5fd85b-98a3-4778-9fc4-26d93ae91d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf7b4e6-c72a-442d-9fc7-fea31c276188",
                    "LayerId": "6e195a5c-907c-46aa-a031-b297c68de06a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "6e195a5c-907c-46aa-a031-b297c68de06a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f001ab6-c951-4a63-815d-49c7fa2607c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 0,
    "yorig": 0
}