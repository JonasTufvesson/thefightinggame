{
    "id": "3537c69a-b583-40a5-831c-e49c740980a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_block_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 7,
    "bbox_right": 52,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "766764da-20e3-448c-8ddf-08c786f165df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3537c69a-b583-40a5-831c-e49c740980a7",
            "compositeImage": {
                "id": "587d1cd6-182d-47f3-8d44-c43b03798feb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "766764da-20e3-448c-8ddf-08c786f165df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541012f9-cd89-4c44-a5d0-c764bf9cf946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "766764da-20e3-448c-8ddf-08c786f165df",
                    "LayerId": "7c09d34a-3120-46b6-b338-8df966c20a92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "7c09d34a-3120-46b6-b338-8df966c20a92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3537c69a-b583-40a5-831c-e49c740980a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 0,
    "yorig": 0
}