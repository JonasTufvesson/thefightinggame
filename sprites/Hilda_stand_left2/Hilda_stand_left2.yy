{
    "id": "72499af8-d38c-4e7b-af93-efcacea85328",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Hilda_stand_left2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 142,
    "bbox_left": 10,
    "bbox_right": 60,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8759e54f-00b7-46af-9ecb-9620c3cc09e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72499af8-d38c-4e7b-af93-efcacea85328",
            "compositeImage": {
                "id": "aed7e1a4-6ebc-4eb6-959b-9d80119aa0f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8759e54f-00b7-46af-9ecb-9620c3cc09e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26fc32c4-a9a2-4ae7-be75-50549c8fb743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8759e54f-00b7-46af-9ecb-9620c3cc09e1",
                    "LayerId": "a83a2e0c-57c7-49a4-ae07-e82b72050a2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "a83a2e0c-57c7-49a4-ae07-e82b72050a2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72499af8-d38c-4e7b-af93-efcacea85328",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 0
}