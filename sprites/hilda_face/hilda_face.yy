{
    "id": "70ae25f3-e866-4be0-9258-2fcd0f860a04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_face",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 1,
    "bbox_right": 93,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfb848fb-7471-4773-b464-0fdf2453f09a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70ae25f3-e866-4be0-9258-2fcd0f860a04",
            "compositeImage": {
                "id": "5d2827e2-fc30-4f0d-9067-274b2302a8cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb848fb-7471-4773-b464-0fdf2453f09a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed2f09f-3a9e-4c90-9549-ca189a57c3c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb848fb-7471-4773-b464-0fdf2453f09a",
                    "LayerId": "e5e6285a-e9d6-4206-88d3-a533b0fc5a40"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 139,
    "layers": [
        {
            "id": "e5e6285a-e9d6-4206-88d3-a533b0fc5a40",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70ae25f3-e866-4be0-9258-2fcd0f860a04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 95,
    "xorig": 0,
    "yorig": 0
}