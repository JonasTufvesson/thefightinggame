{
    "id": "d3bc6fe2-5107-43f7-b0a3-72528d7908f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_run_right3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 5,
    "bbox_right": 65,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1f46cee-4b3a-4222-abea-5f580a0e8a90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3bc6fe2-5107-43f7-b0a3-72528d7908f0",
            "compositeImage": {
                "id": "419979f1-0d82-48b5-ae9f-6cb9b0004ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f46cee-4b3a-4222-abea-5f580a0e8a90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996a4070-aae6-4475-b3e0-a376d40c590a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f46cee-4b3a-4222-abea-5f580a0e8a90",
                    "LayerId": "e017b445-0340-4a3a-9544-27a162b6b8d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "e017b445-0340-4a3a-9544-27a162b6b8d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3bc6fe2-5107-43f7-b0a3-72528d7908f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 0,
    "yorig": 0
}