{
    "id": "e209130e-5dc7-4740-b6bf-01f5806fa32d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_crouch_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 1,
    "bbox_right": 35,
    "bbox_top": 115,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "048f0c60-5b6e-4bf2-92b1-6928c35b3d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e209130e-5dc7-4740-b6bf-01f5806fa32d",
            "compositeImage": {
                "id": "83926e34-0d82-49f3-b401-e5f9895d0911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048f0c60-5b6e-4bf2-92b1-6928c35b3d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc0bb1e3-96ea-46e3-a5dd-02b8e57007ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048f0c60-5b6e-4bf2-92b1-6928c35b3d9d",
                    "LayerId": "c1c21c3f-cfc2-4dfb-b03f-d7c1c0e0b1da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "c1c21c3f-cfc2-4dfb-b03f-d7c1c0e0b1da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e209130e-5dc7-4740-b6bf-01f5806fa32d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 41,
    "xorig": 0,
    "yorig": 0
}