{
    "id": "bfb59790-72ca-4ac8-9223-db3ed61679cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_run_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 2,
    "bbox_right": 66,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "413ae49f-fa4f-49a9-95eb-b164eb443826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfb59790-72ca-4ac8-9223-db3ed61679cb",
            "compositeImage": {
                "id": "411daf47-deb1-4d0d-9dd2-81b2b3fd4d89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "413ae49f-fa4f-49a9-95eb-b164eb443826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31309cac-119e-4d4e-b9a5-5230c6ace0f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "413ae49f-fa4f-49a9-95eb-b164eb443826",
                    "LayerId": "8493749f-6be4-4422-9ec7-13e3464d875b"
                }
            ]
        },
        {
            "id": "7cc4a39b-3600-441c-a0dd-fcbaeadef327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfb59790-72ca-4ac8-9223-db3ed61679cb",
            "compositeImage": {
                "id": "c869db25-1d80-4a22-a3a3-08aaa5302b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc4a39b-3600-441c-a0dd-fcbaeadef327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d52f77-5b5d-4658-9097-53a99e9d0eec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc4a39b-3600-441c-a0dd-fcbaeadef327",
                    "LayerId": "8493749f-6be4-4422-9ec7-13e3464d875b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "8493749f-6be4-4422-9ec7-13e3464d875b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfb59790-72ca-4ac8-9223-db3ed61679cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 0,
    "yorig": 0
}