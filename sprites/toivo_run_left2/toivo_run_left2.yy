{
    "id": "7808f9b0-3c45-4776-ac61-ec1a94dc59c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_run_left2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 3,
    "bbox_right": 66,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "765f3b63-13f8-4c6d-8edf-174b92f04511",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7808f9b0-3c45-4776-ac61-ec1a94dc59c4",
            "compositeImage": {
                "id": "cae6d158-4e29-41de-ad75-cfc59efaf109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "765f3b63-13f8-4c6d-8edf-174b92f04511",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d1d8d63-3cbf-4b68-a7e5-cfbe21faf1ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "765f3b63-13f8-4c6d-8edf-174b92f04511",
                    "LayerId": "53c13882-9eb3-4017-adc5-44b16ee26e5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "53c13882-9eb3-4017-adc5-44b16ee26e5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7808f9b0-3c45-4776-ac61-ec1a94dc59c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 0,
    "yorig": 0
}