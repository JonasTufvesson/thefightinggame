{
    "id": "4aadfdd6-86e5-4183-aaab-bd15ea50bed6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_right3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 2,
    "bbox_right": 52,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10ab067c-fb82-413a-8c8f-0a4083171c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4aadfdd6-86e5-4183-aaab-bd15ea50bed6",
            "compositeImage": {
                "id": "0f89aa4b-775c-4ede-97ab-f44deed8c8de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10ab067c-fb82-413a-8c8f-0a4083171c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4d312b1-fcdc-40be-b912-8ce881fd2f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10ab067c-fb82-413a-8c8f-0a4083171c92",
                    "LayerId": "f88a8965-cf1b-46fd-85d8-132d57a26dae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "f88a8965-cf1b-46fd-85d8-132d57a26dae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4aadfdd6-86e5-4183-aaab-bd15ea50bed6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 0,
    "yorig": 0
}