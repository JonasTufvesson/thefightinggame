{
    "id": "b89f3863-ff62-4cd6-a5ef-0e851eb82679",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FAce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 134,
    "bbox_left": 88,
    "bbox_right": 144,
    "bbox_top": 85,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d6991d6-9af1-4b7d-afad-94ed0b952bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b89f3863-ff62-4cd6-a5ef-0e851eb82679",
            "compositeImage": {
                "id": "b0dedc1a-e653-4e66-a104-a9f29638b0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6991d6-9af1-4b7d-afad-94ed0b952bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9450ad-de76-4703-a7c2-a5afac312c91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6991d6-9af1-4b7d-afad-94ed0b952bc3",
                    "LayerId": "e7449aed-3202-4d03-b58f-8f140de927e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e7449aed-3202-4d03-b58f-8f140de927e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b89f3863-ff62-4cd6-a5ef-0e851eb82679",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}