{
    "id": "0f3cd9de-4eeb-468d-bde4-61523c451b6b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_punch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 53,
    "bbox_right": 92,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbb1e65b-0963-4207-a25f-82f1aa7ca545",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f3cd9de-4eeb-468d-bde4-61523c451b6b",
            "compositeImage": {
                "id": "3f7452af-f0c3-4280-a46e-f590c9df067a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb1e65b-0963-4207-a25f-82f1aa7ca545",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abde91c4-2a58-43ee-8f45-2d92323e23d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb1e65b-0963-4207-a25f-82f1aa7ca545",
                    "LayerId": "cb3787c4-3b65-4cb1-98ba-64d1ab1a980f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "cb3787c4-3b65-4cb1-98ba-64d1ab1a980f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f3cd9de-4eeb-468d-bde4-61523c451b6b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 93,
    "xorig": 46,
    "yorig": 0
}