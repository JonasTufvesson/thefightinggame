{
    "id": "93c689c4-7c87-453b-89e2-7e026c0246d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_stand_right1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 156,
    "bbox_left": 5,
    "bbox_right": 52,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92efabf1-619a-45c3-8065-f034080d2c4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93c689c4-7c87-453b-89e2-7e026c0246d9",
            "compositeImage": {
                "id": "74ba8ef4-ae2e-4e25-bbad-7f8e0a721cb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92efabf1-619a-45c3-8065-f034080d2c4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e53de67e-06b0-45ef-a36f-51fbf1e9f6c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92efabf1-619a-45c3-8065-f034080d2c4c",
                    "LayerId": "025aa434-5206-45de-af1b-15c3d5fcbda8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "025aa434-5206-45de-af1b-15c3d5fcbda8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93c689c4-7c87-453b-89e2-7e026c0246d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 55,
    "xorig": 0,
    "yorig": 0
}