{
    "id": "289271d2-dd4d-4933-a6fc-ef7d691728d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da48d923-09eb-4fa8-aac5-27ce4ca99996",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "289271d2-dd4d-4933-a6fc-ef7d691728d2",
            "compositeImage": {
                "id": "a2bdc241-ede5-4346-84ab-5b72162cd6fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da48d923-09eb-4fa8-aac5-27ce4ca99996",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3363f7d0-31f4-4d43-92ad-52a3cfaa3ddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da48d923-09eb-4fa8-aac5-27ce4ca99996",
                    "LayerId": "784693d9-77f3-4b00-8b86-5ac431119f89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "784693d9-77f3-4b00-8b86-5ac431119f89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "289271d2-dd4d-4933-a6fc-ef7d691728d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}