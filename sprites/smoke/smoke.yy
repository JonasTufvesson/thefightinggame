{
    "id": "aa74c004-871d-4e42-a08b-c94c58d73c30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23e28f3f-84fa-41f9-b500-fa5d06fdf24b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "448f27a4-6615-4b02-8d1d-bfaad5866c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23e28f3f-84fa-41f9-b500-fa5d06fdf24b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a125562-d751-4672-8f3e-c6bc94595634",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23e28f3f-84fa-41f9-b500-fa5d06fdf24b",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "6d1adb08-a221-454f-b25f-0e836660f5d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "508a6c7a-b68e-4b68-b31a-4983baafd435",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d1adb08-a221-454f-b25f-0e836660f5d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d537a4f-ffd4-41e8-8f0f-cd4b9d5a6dea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d1adb08-a221-454f-b25f-0e836660f5d0",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "b39d47ad-d0cb-4194-8018-e2e7184512b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "f682e41b-5a1d-4cf0-86f4-5a670fe89b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b39d47ad-d0cb-4194-8018-e2e7184512b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "949ca6da-b404-44bd-91be-cea6f6267f2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b39d47ad-d0cb-4194-8018-e2e7184512b4",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "99e91d9d-9abf-4ea6-bfb7-217b5aef46a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "433c8217-5157-4d75-ab18-0079417322c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e91d9d-9abf-4ea6-bfb7-217b5aef46a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2ecc3a5-b786-4de6-afcc-b4588548d344",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e91d9d-9abf-4ea6-bfb7-217b5aef46a3",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "e175a923-5ca9-4262-b4a9-113632750b2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "58092745-d7aa-4045-b7dc-e7658ead1d2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e175a923-5ca9-4262-b4a9-113632750b2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f9a80f-8522-4564-8118-d94041d86d01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e175a923-5ca9-4262-b4a9-113632750b2e",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "649ea940-d68d-4fec-8360-8b55a37ffcd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "d496e765-f3b5-4746-8677-391d49816628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "649ea940-d68d-4fec-8360-8b55a37ffcd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "817ca5ec-32d2-4ae3-a8e3-c4cb7baf922e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "649ea940-d68d-4fec-8360-8b55a37ffcd1",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "50f0fbdf-05a3-4353-aff1-20a49ff65f86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "7cbbd9eb-7688-477d-8d1b-0d92f290c42c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f0fbdf-05a3-4353-aff1-20a49ff65f86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e6252e-60af-47bb-b6b1-070a695ca1d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f0fbdf-05a3-4353-aff1-20a49ff65f86",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "c9b03325-6595-4acb-9d55-59d7acb61ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "2e06f594-84c9-4e6b-95ab-844a47ccd311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b03325-6595-4acb-9d55-59d7acb61ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f6c4c09-6b01-4272-8f7c-810e07e9f351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b03325-6595-4acb-9d55-59d7acb61ff7",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "fc2c6563-3642-48ba-8fff-5dcc52077f6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "e2233483-987e-4dc7-9800-ee0f854d087e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc2c6563-3642-48ba-8fff-5dcc52077f6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "000fde74-7876-46d0-923c-4ddaedb38e52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc2c6563-3642-48ba-8fff-5dcc52077f6f",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "1207570a-231c-426c-9d4a-f578ead73f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "0efcaaff-a621-4216-a6bf-2f00eb81babb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1207570a-231c-426c-9d4a-f578ead73f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6056dcb9-17fa-4e47-89fd-24091e52ef82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1207570a-231c-426c-9d4a-f578ead73f4d",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "2cfbb3c3-e3b0-4746-bd09-a2198acb6d38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "4a5fec9a-3bb9-41e9-ba8c-c92d12c8cb9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cfbb3c3-e3b0-4746-bd09-a2198acb6d38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60239fba-6050-4ce5-b1c9-6c757659016b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cfbb3c3-e3b0-4746-bd09-a2198acb6d38",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "c73fb4c1-4856-465a-8721-4e67f9b6cd2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "9dd9beff-8f0f-4da8-b858-b337c893e982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73fb4c1-4856-465a-8721-4e67f9b6cd2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "754c3aa4-e5f8-48dd-8212-65b9d153966f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73fb4c1-4856-465a-8721-4e67f9b6cd2c",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        },
        {
            "id": "3b5117b1-c188-425c-bddc-4a74333c8f67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "compositeImage": {
                "id": "30224210-08a7-42b5-b860-ae4cd470eb32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b5117b1-c188-425c-bddc-4a74333c8f67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "082bbdaa-3533-4ecb-87da-e534dd68a154",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b5117b1-c188-425c-bddc-4a74333c8f67",
                    "LayerId": "5670ef5b-ea31-491d-940b-ec4a6a9b5827"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5670ef5b-ea31-491d-940b-ec4a6a9b5827",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa74c004-871d-4e42-a08b-c94c58d73c30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}