{
    "id": "7f93f490-5a4a-4e17-8866-b377b189c51f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_stand_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 0,
    "bbox_right": 42,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f5c1e73-6bc2-4014-9a6c-21bcc614900d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f93f490-5a4a-4e17-8866-b377b189c51f",
            "compositeImage": {
                "id": "09dbec36-ef80-4195-a90b-fba510fcb1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5c1e73-6bc2-4014-9a6c-21bcc614900d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7788b2a5-6d61-431d-82d2-14dd4a6f3f8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5c1e73-6bc2-4014-9a6c-21bcc614900d",
                    "LayerId": "9d29d83c-5cd1-40ac-86f7-0db5f500bbe9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "9d29d83c-5cd1-40ac-86f7-0db5f500bbe9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f93f490-5a4a-4e17-8866-b377b189c51f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 43,
    "xorig": 0,
    "yorig": 0
}