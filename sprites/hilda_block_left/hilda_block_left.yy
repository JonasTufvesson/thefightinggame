{
    "id": "f1024227-b66b-4c02-9457-c9623cd134d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_block_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 8,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c783283-b536-4c3d-be18-7c62860f22ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1024227-b66b-4c02-9457-c9623cd134d0",
            "compositeImage": {
                "id": "dbd782d1-6edb-43a7-a11a-9b8254dd50ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c783283-b536-4c3d-be18-7c62860f22ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93081ef8-c9a7-4d7f-9ae9-d3c7ff492b39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c783283-b536-4c3d-be18-7c62860f22ff",
                    "LayerId": "1612d1ea-62a3-4b30-b21f-1b18b4bd6ba9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "1612d1ea-62a3-4b30-b21f-1b18b4bd6ba9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1024227-b66b-4c02-9457-c9623cd134d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}