{
    "id": "950b75e0-eff8-454c-9054-2a4b414623a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_punch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 161,
    "bbox_left": 61,
    "bbox_right": 107,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2a9643c-dcdb-45aa-bd67-5c2eba22619b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "950b75e0-eff8-454c-9054-2a4b414623a0",
            "compositeImage": {
                "id": "7912a401-7198-4a29-8c39-2fb41f88891f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a9643c-dcdb-45aa-bd67-5c2eba22619b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fe0fc67-932c-4b16-90b0-5a9f5838436a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a9643c-dcdb-45aa-bd67-5c2eba22619b",
                    "LayerId": "8b625145-8040-41fb-8531-cc670a0e6c92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "8b625145-8040-41fb-8531-cc670a0e6c92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "950b75e0-eff8-454c-9054-2a4b414623a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 117,
    "xorig": 58,
    "yorig": 0
}