{
    "id": "81b5f2a9-00f0-4f93-a8ec-dd76ca03bf28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_stand_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 142,
    "bbox_left": 18,
    "bbox_right": 68,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1420acd-0d95-4c10-9a7e-555761e9a06c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81b5f2a9-00f0-4f93-a8ec-dd76ca03bf28",
            "compositeImage": {
                "id": "7d0b5c5d-4852-4177-a6c8-24a0978af487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1420acd-0d95-4c10-9a7e-555761e9a06c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3a392cf-2db6-45f9-a7f2-8720ad74c872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1420acd-0d95-4c10-9a7e-555761e9a06c",
                    "LayerId": "232e7cda-5e26-4991-bdd5-c112c827f68f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "232e7cda-5e26-4991-bdd5-c112c827f68f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81b5f2a9-00f0-4f93-a8ec-dd76ca03bf28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 0
}