{
    "id": "bc8ebf16-501c-4419-a3f5-10bdd569017b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_stand_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 5,
    "bbox_right": 45,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05df8e49-c683-4747-89ab-6b22aa24ddc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc8ebf16-501c-4419-a3f5-10bdd569017b",
            "compositeImage": {
                "id": "49db2ba6-1c4f-4354-838c-70f9b480126b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05df8e49-c683-4747-89ab-6b22aa24ddc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "215c8271-d9f6-4a34-b15f-e00f3eb57f5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05df8e49-c683-4747-89ab-6b22aa24ddc9",
                    "LayerId": "b9527d05-fe5a-4c64-9224-782ac4ba6289"
                },
                {
                    "id": "aa3d2711-46fa-4cce-a992-ca55fa649bb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05df8e49-c683-4747-89ab-6b22aa24ddc9",
                    "LayerId": "77d7ab1b-dfd8-48b3-bdc7-a66cfbf31f04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "77d7ab1b-dfd8-48b3-bdc7-a66cfbf31f04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc8ebf16-501c-4419-a3f5-10bdd569017b",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b9527d05-fe5a-4c64-9224-782ac4ba6289",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc8ebf16-501c-4419-a3f5-10bdd569017b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 0,
    "yorig": 0
}