{
    "id": "a9505cab-747f-461f-ba5a-ca9302b415b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "punch_kick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ee95d50-67e3-46de-b8c5-3581b9339425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9505cab-747f-461f-ba5a-ca9302b415b0",
            "compositeImage": {
                "id": "a7acb7ec-4170-4136-b8cc-970582d345fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee95d50-67e3-46de-b8c5-3581b9339425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee56869f-4f30-4f5c-88a9-459376d2523e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee95d50-67e3-46de-b8c5-3581b9339425",
                    "LayerId": "b2f6f7bf-a909-4df2-ada1-f44c3eafbe74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b2f6f7bf-a909-4df2-ada1-f44c3eafbe74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9505cab-747f-461f-ba5a-ca9302b415b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}