{
    "id": "4f63681e-fa4d-4619-a688-ee785e221248",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 24,
    "bbox_right": 239,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5258b348-bd44-4128-8a93-19ab236d0454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "736ce566-4fb4-42d3-b374-3c19267f5cc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5258b348-bd44-4128-8a93-19ab236d0454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe4300f3-c25d-4e18-9bd1-e0f34bd48d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5258b348-bd44-4128-8a93-19ab236d0454",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "76e89132-1e3d-4a4f-9325-087e4865c646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "de0c2377-e501-4728-a459-5b237ba9bfcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e89132-1e3d-4a4f-9325-087e4865c646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76cc14cd-2490-4e88-b4d3-25f8fee4ac68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e89132-1e3d-4a4f-9325-087e4865c646",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "1251b551-1526-401e-b869-c06d29123f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "1f2eab77-df90-4a91-a954-bdb480ef4e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1251b551-1526-401e-b869-c06d29123f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d813f622-0c46-4b03-9b9a-64d5f362515d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1251b551-1526-401e-b869-c06d29123f2c",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "a18ab70e-aa16-40c2-aba5-65fb9b4d7eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "9ee8f109-d7b9-4a0b-aacf-39e3f65036b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a18ab70e-aa16-40c2-aba5-65fb9b4d7eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e3e067-6b77-4915-9b41-a5751752bd77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a18ab70e-aa16-40c2-aba5-65fb9b4d7eaf",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "d7f6e8fa-566f-408b-be20-2e5053966988",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "09ae879b-70e6-41c9-988f-89a87ecee58d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f6e8fa-566f-408b-be20-2e5053966988",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b01226-4058-4979-ac45-9241954c0f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f6e8fa-566f-408b-be20-2e5053966988",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "9c99fb23-7f1a-49bb-8faa-7c82cacab991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "347758d4-6c21-4623-bdf3-b8530e712dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c99fb23-7f1a-49bb-8faa-7c82cacab991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf3407b2-bbb0-448a-b7ee-93e32f7373c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c99fb23-7f1a-49bb-8faa-7c82cacab991",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "744598c0-dc56-429d-b9cb-542425d6e6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "79969d57-127d-449b-b131-6f1dc36473d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "744598c0-dc56-429d-b9cb-542425d6e6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39ec6092-6908-4fc3-b510-e4184fd4a68d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "744598c0-dc56-429d-b9cb-542425d6e6e4",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        },
        {
            "id": "d33d44f7-09e0-4bd5-9faf-5bbc23999fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "compositeImage": {
                "id": "ca55ed52-19a5-4767-8059-5fe691f5d071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33d44f7-09e0-4bd5-9faf-5bbc23999fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0dc4f2-90fe-4a9e-9f7c-fe25a5762455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33d44f7-09e0-4bd5-9faf-5bbc23999fa2",
                    "LayerId": "199ebaf4-6849-474a-ac5f-5e9bdacdf850"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "199ebaf4-6849-474a-ac5f-5e9bdacdf850",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f63681e-fa4d-4619-a688-ee785e221248",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}