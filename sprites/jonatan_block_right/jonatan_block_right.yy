{
    "id": "8d627bd3-0edb-4afa-bc67-c4d740e92991",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_block_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 13,
    "bbox_right": 55,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51902a75-460d-46df-9d8b-cc09d19ae449",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d627bd3-0edb-4afa-bc67-c4d740e92991",
            "compositeImage": {
                "id": "bdedc183-3d64-4075-8777-ce8824a01247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51902a75-460d-46df-9d8b-cc09d19ae449",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d622bf1-bf8a-4929-818e-c10d470493da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51902a75-460d-46df-9d8b-cc09d19ae449",
                    "LayerId": "3ec21f10-d19d-4525-8941-11150d72a299"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "3ec21f10-d19d-4525-8941-11150d72a299",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d627bd3-0edb-4afa-bc67-c4d740e92991",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}