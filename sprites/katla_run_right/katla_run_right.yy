{
    "id": "b6da2802-57c6-4902-bf8c-c1de4bfa39b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 154,
    "bbox_left": 22,
    "bbox_right": 58,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13c5aafc-d277-4534-a53a-d13a2df30078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6da2802-57c6-4902-bf8c-c1de4bfa39b8",
            "compositeImage": {
                "id": "825fe775-2d57-4081-a771-0b1828a3af81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13c5aafc-d277-4534-a53a-d13a2df30078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4529c2bb-0aed-4bbb-b755-1d678d52ac4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13c5aafc-d277-4534-a53a-d13a2df30078",
                    "LayerId": "0cbece40-2bea-4339-8b07-9febfe591acd"
                }
            ]
        },
        {
            "id": "192a38df-a9d4-45f7-a2e3-7563255f9258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6da2802-57c6-4902-bf8c-c1de4bfa39b8",
            "compositeImage": {
                "id": "8b69f02e-db36-43b8-a297-9b50b14589ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "192a38df-a9d4-45f7-a2e3-7563255f9258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab89e12-0cdb-4789-a38b-e98b2a303e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "192a38df-a9d4-45f7-a2e3-7563255f9258",
                    "LayerId": "0cbece40-2bea-4339-8b07-9febfe591acd"
                }
            ]
        },
        {
            "id": "52b57822-913f-492e-9aa5-33d5269798f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6da2802-57c6-4902-bf8c-c1de4bfa39b8",
            "compositeImage": {
                "id": "75b5911a-a09d-46bb-b252-b9bb3c33f44a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b57822-913f-492e-9aa5-33d5269798f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8223a6-332e-46bb-ab52-dc3673ac4d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b57822-913f-492e-9aa5-33d5269798f2",
                    "LayerId": "0cbece40-2bea-4339-8b07-9febfe591acd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "0cbece40-2bea-4339-8b07-9febfe591acd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6da2802-57c6-4902-bf8c-c1de4bfa39b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 79,
    "xorig": 0,
    "yorig": 0
}