{
    "id": "87bc26e7-c6b6-47c6-8318-cb21a9ae41c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_block_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 1,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82aed30d-a012-4e85-8123-48dc47f8b614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87bc26e7-c6b6-47c6-8318-cb21a9ae41c1",
            "compositeImage": {
                "id": "ba0360ae-5a8e-443a-a224-10bde61a71f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82aed30d-a012-4e85-8123-48dc47f8b614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49969dc1-83a3-45bf-8dec-a276fc45a94b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82aed30d-a012-4e85-8123-48dc47f8b614",
                    "LayerId": "49300643-5e6d-429d-b73a-c635b6639ed3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "49300643-5e6d-429d-b73a-c635b6639ed3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87bc26e7-c6b6-47c6-8318-cb21a9ae41c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}