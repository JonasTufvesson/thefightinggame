{
    "id": "68b9a9c0-d505-4555-8eed-aa06dd25c2a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Hilda_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 149,
    "bbox_left": 11,
    "bbox_right": 61,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "205482ec-e066-4569-838a-f95236fc9d21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b9a9c0-d505-4555-8eed-aa06dd25c2a1",
            "compositeImage": {
                "id": "d0a6a461-17d4-4165-9590-9d1cf7d08ad0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "205482ec-e066-4569-838a-f95236fc9d21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79034626-be0d-48c5-9604-51d041b93d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "205482ec-e066-4569-838a-f95236fc9d21",
                    "LayerId": "7308b6b7-db7e-4474-b1fd-78f109c3ca65"
                }
            ]
        },
        {
            "id": "eb419ce5-1e06-49c9-b315-2dd8285d8eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68b9a9c0-d505-4555-8eed-aa06dd25c2a1",
            "compositeImage": {
                "id": "2b7ce225-7876-4534-a63a-21daeb439f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb419ce5-1e06-49c9-b315-2dd8285d8eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06771414-3176-47bf-bd6a-29fb58f8f1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb419ce5-1e06-49c9-b315-2dd8285d8eee",
                    "LayerId": "7308b6b7-db7e-4474-b1fd-78f109c3ca65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "7308b6b7-db7e-4474-b1fd-78f109c3ca65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68b9a9c0-d505-4555-8eed-aa06dd25c2a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}