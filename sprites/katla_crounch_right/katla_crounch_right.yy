{
    "id": "ba20ff78-66be-442b-bbd1-150eeebc7cfb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "katla_crounch_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 160,
    "bbox_left": 3,
    "bbox_right": 47,
    "bbox_top": 115,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bdd2262-7546-42f7-953f-17c8a8912325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba20ff78-66be-442b-bbd1-150eeebc7cfb",
            "compositeImage": {
                "id": "d92e121b-8238-43de-b6c2-61bbac0281ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bdd2262-7546-42f7-953f-17c8a8912325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53935934-9d4c-4f9d-8a2d-9759ac8b3bad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bdd2262-7546-42f7-953f-17c8a8912325",
                    "LayerId": "b61c2f1d-a753-4f31-ad19-c896b1b46ae2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "b61c2f1d-a753-4f31-ad19-c896b1b46ae2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba20ff78-66be-442b-bbd1-150eeebc7cfb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 0,
    "yorig": 0
}