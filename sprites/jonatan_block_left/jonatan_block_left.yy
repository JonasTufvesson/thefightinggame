{
    "id": "19f4229b-628f-4113-b282-cfddf0c95b17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jonatan_block_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 11,
    "bbox_right": 54,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad5698ad-0dac-4cfd-ac60-796840bca0cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19f4229b-628f-4113-b282-cfddf0c95b17",
            "compositeImage": {
                "id": "697bd5f8-18ca-4a3c-88e4-75036692b442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad5698ad-0dac-4cfd-ac60-796840bca0cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12cd77d3-ba87-4f44-86af-ed7ce0e2e838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad5698ad-0dac-4cfd-ac60-796840bca0cc",
                    "LayerId": "dc0b6369-6da5-4eba-946f-674312892a88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "dc0b6369-6da5-4eba-946f-674312892a88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19f4229b-628f-4113-b282-cfddf0c95b17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 65,
    "xorig": 0,
    "yorig": 0
}