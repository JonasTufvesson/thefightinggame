{
    "id": "b5526459-e0b1-4d07-8271-f2daa87c6cc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hilda_kick_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 158,
    "bbox_left": 7,
    "bbox_right": 59,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4e1818f-23a0-46f3-bbc1-9b9c13393122",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5526459-e0b1-4d07-8271-f2daa87c6cc5",
            "compositeImage": {
                "id": "8bf25204-7fd9-43b8-9ea1-f166105711c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4e1818f-23a0-46f3-bbc1-9b9c13393122",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb096205-1ab0-45bb-9c3c-95bbc069ef11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4e1818f-23a0-46f3-bbc1-9b9c13393122",
                    "LayerId": "4ad7f42c-7908-4945-9b84-72a17575ad7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 161,
    "layers": [
        {
            "id": "4ad7f42c-7908-4945-9b84-72a17575ad7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5526459-e0b1-4d07-8271-f2daa87c6cc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 0,
    "yorig": 0
}