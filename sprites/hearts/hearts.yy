{
    "id": "42945205-45fb-4d82-b41d-8fb52cd52826",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hearts",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43ec19d4-4834-4493-a196-f480408f5741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42945205-45fb-4d82-b41d-8fb52cd52826",
            "compositeImage": {
                "id": "4349a7d1-d260-4d78-979c-b76a21bc4026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ec19d4-4834-4493-a196-f480408f5741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad0e3239-9b81-408d-96fa-75ca62938a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ec19d4-4834-4493-a196-f480408f5741",
                    "LayerId": "bbee0669-ff41-47f3-b0b7-3c8c61bcce52"
                }
            ]
        },
        {
            "id": "0e029889-71ca-4471-9727-3c251a87e8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42945205-45fb-4d82-b41d-8fb52cd52826",
            "compositeImage": {
                "id": "4fe46e8e-402f-4515-b523-028615de871a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e029889-71ca-4471-9727-3c251a87e8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6784f7d5-b195-4326-a39a-54bbdf666843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e029889-71ca-4471-9727-3c251a87e8ea",
                    "LayerId": "bbee0669-ff41-47f3-b0b7-3c8c61bcce52"
                }
            ]
        },
        {
            "id": "a43f5d5c-811f-416d-9279-8f695f5eb3b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42945205-45fb-4d82-b41d-8fb52cd52826",
            "compositeImage": {
                "id": "758d1280-28e6-405e-a703-a2fdb103da6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a43f5d5c-811f-416d-9279-8f695f5eb3b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a207c99c-f4b2-4778-87ba-42d4d9a94612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a43f5d5c-811f-416d-9279-8f695f5eb3b6",
                    "LayerId": "bbee0669-ff41-47f3-b0b7-3c8c61bcce52"
                }
            ]
        },
        {
            "id": "c77ef192-ebdb-4cab-be0c-7a21efb5b131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42945205-45fb-4d82-b41d-8fb52cd52826",
            "compositeImage": {
                "id": "a114e2ef-5eb7-4345-8010-8f04e0260c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c77ef192-ebdb-4cab-be0c-7a21efb5b131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80fd8b1e-7cdd-4268-926a-840eef06ce04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c77ef192-ebdb-4cab-be0c-7a21efb5b131",
                    "LayerId": "bbee0669-ff41-47f3-b0b7-3c8c61bcce52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bbee0669-ff41-47f3-b0b7-3c8c61bcce52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42945205-45fb-4d82-b41d-8fb52cd52826",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}