{
    "id": "74894c7d-62c4-45f2-9e52-82fe0dce126a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "toivo_kick_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 68,
    "bbox_right": 119,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1c4da9b-0637-4a21-a638-3afce49f5bc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74894c7d-62c4-45f2-9e52-82fe0dce126a",
            "compositeImage": {
                "id": "76f075ef-89ba-4032-bef4-db775e703683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1c4da9b-0637-4a21-a638-3afce49f5bc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a3f85c-29b5-4618-9021-765b97c3fc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1c4da9b-0637-4a21-a638-3afce49f5bc3",
                    "LayerId": "46b2a64b-b5f7-40e5-962b-dd4689f078b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 159,
    "layers": [
        {
            "id": "46b2a64b-b5f7-40e5-962b-dd4689f078b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74894c7d-62c4-45f2-9e52-82fe0dce126a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 124,
    "xorig": 62,
    "yorig": 0
}